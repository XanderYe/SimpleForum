<%@ page import="com.chinasoft.entity.Topic" %>
<%@ page import="java.util.List" %>
<%@ page import="com.chinasoft.entity.Post" %><%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/5/22
  Time: 14:53
  To change this template use File | Settings | File Templates.
--%>
<%
    String htmlData = request.getParameter("content") != null ? request.getParameter("content") : "";
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>${requestScope.section.sname} 论坛</title>

    <link rel="stylesheet" type="text/css" href="${ctx}/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/css/utils.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/css/mobile.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/css/jquery-ui.css"/>
    <link rel="stylesheet" href="${ctx}/js/themes/default/default.css"/>
    <link rel="stylesheet" href="${ctx}/css/prettify.css"/>
    <script src="${ctx}/js/jquery-3.3.1.js"></script>
    <script src="${ctx}/js/require.js"></script>
    <script src="${ctx}/js/utils.js"></script>
    <script src="${ctx}/js/app.js"></script>
    <script charset="utf-8" src="${ctx}/js/kindeditor-all.js"></script>
    <script charset="utf-8" src="${ctx}/js/zh-CN.js"></script>
    <script charset="utf-8" src="${ctx}/js/prettify.js"></script>
    <script src="${ctx}/js/forum.js"></script>
    <script src="${ctx}/js/post.js"></script>
    <style type="text/css">
        h3 {
            font-size: 16px;
        }
    </style>
</head>
<body>
<c:choose>
    <c:when test="${empty param.page}">
        <c:set var="page" value="1"></c:set>
    </c:when>
    <c:otherwise>
        <c:set var="page" value="${param.page}"></c:set>
    </c:otherwise>
</c:choose>
<c:choose>
    <c:when test="${empty param.tid}">
        <c:set var="tid" value="0"></c:set>
    </c:when>
    <c:otherwise>
        <c:set var="tid" value="${param.tid}"></c:set>
    </c:otherwise>
</c:choose>
<c:if test="${empty param.sid}">
    <jsp:forward page="forum.jsp"></jsp:forward>
</c:if>
<c:if test="${empty requestScope.section}">
    <jsp:forward page="post?postop=getposts&sid=${param.sid}&tid=${tid}&page=${page}"></jsp:forward>
</c:if>
<c:set var="maxpage" value="${requestScope.maxpage}"></c:set>
<c:set var="posts" value="${requestScope.posts}"></c:set>
<div class="sid" sid="${requestScope.section.sid}"></div>
<div class="uid" uid="${sessionScope.user.uid}"></div>
<div class="pop-dialog" id="popQR">
    <div id="popMask" class="dialog-mask" style="display: block;"></div>
</div>

<!-- 兼容旧版 start -->
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<!-- 兼容旧版 end -->

<div id="toptb" class="cl" style="display: none;">
    <div class="wp">
        <div class="z">
            <script type="text/javascript">var _speedMark = new Date();</script>
        </div>
        <div class="y">
            <a id="switchblind" href="javascript:;" onclick="toggleBlind(this)" title="开启辅助访问" class="switchblind">开启辅助访问</a>
        </div>
    </div>
</div>

<div id="qmenu_menu" class="p_pop blk" style="display: none;">
    <div class="ptm pbw hm">
        请 <a href="javascript:;" class="xi2" onclick="lsSubmit()"><strong>登录</strong></a> 后使用快捷导航<br/>没有帐号？<a
            href="member.php?mod=register" class="xi2 xw1">立即注册</a>
    </div>
</div>
<div class="header-sm">
    <div class="header-main">
        <div class="container-sm clearfix">

            <!-- use login status -->
            <div class="user">
                <c:choose>
                    <c:when test="${empty sessionScope.user}">
                        <form autocomplete="off" id="lsform" class="ui-member">
                            <div class="fastlg cl">
                                <div>
                                    <span class="xi2">
                                        <a id="user-login" href="${ctx}/login.jsp" class="xi2">登录</a>
                                    </span>
                                    <span class="pipe">|</span>
                                    <span class="xi2">
                                        <a id="user-register" href="${ctx}/register.jsp">注册</a>
                                    </span>
                                </div>

                            </div>
                        </form>
                    </c:when>
                    <c:otherwise>
                        <div id="um">
                            <div class="ui-login-toggle">
                                <span class="user-avatar"><img src="${ctx}/avatar/${sessionScope.user.avatar}"/></span>
                                <span class="user-name hide-row">${sessionScope.user.nickname}</span>
                            </div>
                            <div class="ui-login-status">
                                <ul>
                                    <li class="user-primary-info">
                                        <p class="user-avatar-name">
                                    <span class="user-avatar">
                                        <a href="javascript:;"><img src="${ctx}/avatar/${sessionScope.user.avatar}"/></a>
                                    </span> <span class="hide-row">
                                    <a href="javascript:;" target="_self"
                                       class="hide-row">${sessionScope.user.nickname}</a></span>
                                        </p>
                                        <p class="user-pipe"></p>
                                    </li>
                                    <c:if test="${sessionScope.user.permission==99}">
                                        <li class="user-setting"><a href="${ctx}/admin.jsp">后台管理</a></li>
                                    </c:if>
                                    <li class="user-message"><a href="${ctx}/home.jsp?uid=${sessionScope.user.uid}">个人主页</a>
                                    </li>
                                    <li class="user-setting"><a href="${ctx}/setting.jsp">个人设置</a></li>
                                    <li class="user-logout"><a href="${ctx}/user?userop=logout">退出账号</a></li>
                                </ul>
                            </div>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>

            <ul class="header-menu">
                <li><a target="_self" href="${ctx}/index.jsp">论坛首页</a></li>
                <li><a target="_self" href="${ctx}/forum.jsp">论坛版块</a></li>
            </ul>

            <!--<div class="user">
            <img src="img/user-portrait.png">
            <h5>HarryZhang</h5>
            </div>-->
        </div>

        <!-- important { -->
        <div class="p_pop h_pop" id="mn_userapp_menu" style="display: none"></div><!-- } -->


    </div>


    <div class="location">
        <div class="container-sm clearfix">
            <div id="scbar" class="cl">
            </div>
            <ul id="scbar_type_menu" class="p_pop" style="display: none;">
                <li><a href="javascript:;" rel="curforum" fid="189">本版</a></li>
                <li><a href="javascript:;" rel="user">用户</a></li>
            </ul>
            <h2>
                <a href="${ctx}/index.jsp">论坛首页</a> <em>&rsaquo;</em> <a href="${ctx}/forum.jsp">综合区</a><em>&rsaquo;</em>
                <a href="${ctx}/post.jsp?sid=${requestScope.section.sid}">${requestScope.section.sname}</a>
                <div class="z"></div>
            </h2>
        </div>
    </div>

</div>

<div id="main" class="main forums-page clearfix" data-page="forumdisplay">
    <div class="container-sm">
        <div class="section">
            <div class="content">
                <div class="forum-list-panel">
                    <div class="forum-list-panel-top">


                        <ul class="forum-list-panel-category">

                            <li id="ttp_all" class="on">
                                <a class="topictag" tid="0" href="${ctx}/post.jsp?sid=${param.sid}">全部标签</a></li>


                        </ul>


                        <!-- *************** 帖子分类 end **************** -->


                        <div class="element-select">


                        </div>
                        <button id="filter-post-btn" class="btn-post js-needLogin-btn">发布新帖</button>


                    </div>
                    <div class="forum-list-panel-bottom">
                        <div class="forum-list-panel-filter clearfix">
                            <span>筛选：</span>
                            <div class="element-select">
                                <div class="element-select-current">
                                    <a id="filter_special" href="javascript:;">
                                        全部主题
                                    </a>
                                </div>

                                <ul>
                                    <li><a href="javascript:;">全部主题</a></li>

                                </ul>
                            </div>
                        </div>
                        <div class="forum-list-panel-options clearfix">
                            <ul>
                                <li class="on">
                            </ul>
                            </span>
                        </div>
                    </div>
                </div>

                <div id="threadlist">


                    <div>
                        <script type="text/javascript">var lasttime = 1526890718;
                        var listcolspan = '5';</script>
                        <div id="forumnew" style="display:none"></div>
                        <form method="post" autocomplete="off" name="moderate" id="moderate"
                              action="forum.php?mod=topicadmin&amp;action=moderate&amp;fid=189&amp;infloat=yes&amp;nopost=yes">
                            <input type="hidden" name="formhash" value="1c438f4d"/>
                            <input type="hidden" name="listextra" value="page%3D1"/>

                            <input type="hidden" name="optgroup"/>
                            <input type="hidden" name="operation"/>

                            <div class="forum-list-content">

                                <ul id="normalthread_880035" class="js-threadList">
                                    <c:choose>
                                        <c:when test="${empty posts}">
                                            <li>
                                                <a class="forum-list-item-avatar" target="_self" href="#">
                                                    <img src="${ctx}/avatar/defaultavatar.png" alt="">
                                                </a>
                                                <dl class="forum-list-item-summary">
                                                    <dt>
                                                        <h3>
                                                            <a class="forum-list-item-summary-title" href="#" title="">无帖子</a>
                                                        </h3>
                                                    </dt>
                                                    <dd>
                                                        <a href="#" target="_blank"></a>
                                                        <span></span></dd>
                                                </dl>
                                            </li>
                                        </c:when>
                                        <c:otherwise>
                                            <c:forEach var="post" items="${posts}">
                                                <li>
                                                    <a class="forum-list-item-avatar" target="_blank" href="home.jsp?uid=${post.user.uid}">
                                                        <img src="${ctx}/avatar/${post.user.avatar}" alt="${post.user.nickname}">
                                                    </a>
                                                    <dl class="forum-list-item-summary">
                                                        <dt>
                                                            <h3>
                                                                <em>[<a href="${ctx}/post.jsp?sid=${param.sid}&tid=${post.topic.tid}" class="topictag" tid="${post.topic.tid}">${post.topic.tname}</a>]</em>
                                                                <a class="forum-list-item-summary-title" target="_blank" href="comment.jsp?pid=${post.pid}" title="${post.ptitle}">${post.ptitle}</a>
                                                            </h3>
                                                        </dt>
                                                        <dd>
                                                            <a href="${ctx}/home.jsp?uid=${post.user.uid}" target="_blank">${post.user.nickname}</a>
                                                            <span>发表于 ${post.pcreatetime}</span></dd>
                                                    </dl>
                                                </li>
                                            </c:forEach>
                                        </c:otherwise>
                                    </c:choose>
                                </ul>

                            </div>
                        </form>
                    </div>


                </div>


                <div class="forum-list-pager clearfix">

                    <div class="pager-wrapper clearfix">
                        <div class="pg">
                            <c:if test="${page>10}">
                                <a href="${ctx}/post.jsp?sid=${param.sid}&tid=${param.tid}&page=1">1..</a>
                            </c:if>
                            <c:if test="${page>1}">
                                <a href="${ctx}/post.jsp?sid=${param.sid}&tid=${param.tid}&page=${page-1}" class="prev">&nbsp;&nbsp;</a>
                            </c:if>
                            <fmt:parseNumber var="pagediv" integerOnly="true" value="${page/10}" />
                            <c:forEach var="i" begin="${pagediv*10+1}" end="${maxpage<=10?maxpage:(pagediv*10+10)}" step="1">
                                <c:choose>
                                    <c:when test="${i==page}">
                                        <strong>${i}</strong>
                                    </c:when>
                                    <c:when test="${i<=maxpage}">
                                        <a href="${ctx}/post.jsp?sid=${param.sid}&tid=${param.tid}&page=${i}">${i}</a>
                                    </c:when>
                                </c:choose>
                            </c:forEach>
                            <c:if test="${maxpage>10&&maxpage!=page}">
                                <a href="${ctx}/post.jsp?sid=${param.sid}&tid=${param.tid}&page=${maxpage}">..${maxpage}</a>
                            </c:if>
                            <c:if test="${maxpage>page}">
                                <a href="${ctx}/post.jsp?sid=${param.sid}&tid=${param.tid}&page=${page+1}" class="nxt">下一页</a>
                            </c:if>
                    </div>
                </div>

            </div>
            </div>
            <div id="fastpost" class="content forum-list-fastpost">
                <script type="text/javascript">
                    var postminchars = parseInt('2');
                    var postmaxchars = parseInt('200000');
                    var disablepostctrl = parseInt('0');
                    var fid = parseInt('189');
                </script>
                <div class="fastpost">
                    <form method="post" autocomplete="off" id="fastpostform" action="#">

                        <div id="fastpostreturn" style="margin:-5px 0 5px"></div>

                        <div class="fastpost-head">

                            <div class="element-select">

                                <div class="element-select-current">
                                    <a id="topic" href="javascript:;">选择主题分类</a>
                                </div>

                                <ul class="topic">

                                </ul>
                            </div>
                            <div class="fastpost-subject element-input js-subject ">
                                <input type="text" id="subject" name="subject" class="px" value="" placeholder="请填写标题"
                                       tabindex="11" style="width: 528px;height:50px"/>
                                <span>还可输入 <strong id="checklen">90</strong> 个字符</span>
                            </div>

                        </div>

                        <div class="fastpost-content cl">
                            <div id="fastsmiliesdiv" class="y">
                                <div id="fastsmiliesdiv_data">
                                    <div id="fastsmilies"></div>
                                </div>
                            </div>
                            <div class="hasfsl" id="fastposteditor">
                                <div class="tedt" style="height:236px;overflow: hidden;">
                                    <%=htmlData%>
                                    <textarea name="content" class="form-control" cols="100" rows="8"
                                              style="width:720px;height:245px;visibility:hidden;"><%=htmlspecialchars(htmlData)%></textarea>
                                    <br/>
                                </div>
                            </div>
                            <div class="mtm sec">
                                <span id="seccode_cSx63zG7">
                                    验证码
                                    <span>
                                        <input name="seccodeverify" id="seccodeverify" type="text" autocomplete="off"
                                               style="ime-mode:disabled;width:100px" class="txt px vm">
                                        <span id="change"><img src="${ctx}/authImage"><a href="javascript:;"
                                                                                   class="xi2">换一个</a></span>
                                    </span>
                                </span>
                            </div>

                            <input type="hidden" name="formhash" value="1c438f4d"/>
                            <input type="hidden" name="usesig" value=""/>
                        </div>


                        <p class="fastpost-content-bottom ptm pnpost">

                            <button class="btn-post btn-post-B" type="button" name="topicsubmit" id="fastpostsubmit"
                                    value="topicsubmit" tabindex="13" class="pn pnc"><strong>发表帖子</strong></button>
                        </p>
                    </form>
                </div>
            </div>

        </div>

        <!-- *************** 左侧 end **************** -->


        <!-- *************** 右侧模块 start **************** -->
        <div class="sidebar">

            <!-- *************** 用户信息模块 start **************** -->

            <div class="profile">
                <a href="#" class="collect">
                    <i class="icon-fav-star"></i>收藏本版
                </a>
                <div class="profile-card">
<span class="avatar">
<img src="http://bbs-static.smartisan.cn/static/image/forum_icons/big/praise.png" alt=""/>
</span>
                    <p>
                        <span class="name" sid="${requestScope.section.sid}">${requestScope.section.sname}</span>
                    </p>
                    <p class="url">
                        <a></a>
                    </p>
                </div>
                <ul class="profile-col-3 clearfix">
                    <li>
                        <span>今日</span> <em>8</em>
                    </li>
                    <li>
                        <span>主题</span> <em>206</em>
                    </li>
                    <li>
                        <span>排名</span> <em>26</em>
                    </li>
                </ul>
            </div>

            <!-- *************** 用户信息模块 end **************** -->

            <!-- *************** 论坛公告 start **************** -->


            <!-- *************** 广告位 start **************** -->
            <div class="right-module">
                <h4>论坛公告</h4>
                <div class="right-module-notice">
                    <div class="right-module-notice-content">
                        RNG牛逼。<br/>
                        RNG牛逼。<br/>
                        如果有违规内容，相关帖子将会被删除，您的账号也有可能会被警告或禁止发言。详情请查看本版区置顶说明。
                    </div>
                    <dl class="clearfix">
                        <dt>论坛版主：</dt>
                        <dd>
                            <a href="#">
                                <img src="${ctx}/avatar/tx.png" alt="">
                            </a>
                            <a href="${ctx}/home.jsp?uid=1">yzd</a>
                        </dd>


                    </dl>
                </div>
            </div>


            <div class="right-module right-rank-editor">
                <h4>精品推荐</h4>
                <ul class="clearfix">
                    <li class="top3">
                        <a title="#每天大红包#" href="https://www.mtdhb.org" target="_blank">#每天大红包#</a>
                    </li>
                </ul>
            </div>


            <!-- *************** 精品推荐 end **************** -->


            <!-- *************** 版块推荐 start **************** -->


            <div class="right-module plate-recommend">
                <h4>版块推荐</h4>
                <ul class="plate-list clearfix">
                    <li style="background: url(img/ori.png) no-repeat 20px center;background-size: 28px 28px;-webkit-background-size: 28px 28px;">
                        <a href="${ctx}/post.jsp?sid=1" target="_blank">生活</a>
                    </li>

                </ul>
            </div>


            <!-- *************** 版块推荐 end **************** -->

        </div>

        <!-- *************** 右侧模块 end **************** -->

    </div>
</div>
</body>

</html>
<%!
    private String htmlspecialchars(String str) {
        str = str.replaceAll("&", "&amp;");
        str = str.replaceAll("<", "&lt;");
        str = str.replaceAll(">", "&gt;");
        str = str.replaceAll("\"", "&quot;");
        return str;
    }
%>
