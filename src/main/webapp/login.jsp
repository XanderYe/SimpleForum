<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>论坛登录界面</title>
    <link rel="stylesheet" type="text/css" href="${ctx}/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="${ctx}/css/login.css">
    <script src="${ctx}/js/jquery-3.3.1.js"></script>
    <script src="${ctx}/js/bootstrap.min.js"></script>
    <script src="${ctx}/js/login.js"></script>
</head>
<body>
<div class="wrap">
    <div class="container">
        <h1>Welcome</h1>
        <form action="user?userop=login" method="post">
            <div class="input"><input type="text" class="form-control" placeholder="输入用户名" name="username"></div>
            <div class="input"><input type="password" class="form-control" placeholder="输入密码" name="password"></div>
            <div class="input"><input type="submit" class="btn btn-default" value="登录"/></div>
            <div class="input"><input id="register" class="btn btn-default" type="button" value="注册"/></div>
        </form>
    </div>
    <ul>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
    </ul>
</div>
</body>
</html>