<%--
  Created by IntelliJ IDEA.
  User: YE
  Date: 2018/5/22
  Time: 13:30
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>论坛后台管理</title>
    <link rel="stylesheet" type="text/css" href="${ctx}/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/css/utils.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/css/mobile.css"/>
    <script src="js/jquery-3.3.1.js"></script>
    <script src="js/admin.js"></script>
</head>
<body id="nv_home" class="pg_spacecp" onkeydown="if(event.keyCode==27) return false;">
<c:if test="${empty sessionScope.user}">
    <jsp:forward page="${ctx}/login.jsp"></jsp:forward>
</c:if>
<c:if test="${sessionScope.user.permission!=99}">
    <jsp:forward page="${ctx}/index.jsp"></jsp:forward>
</c:if>
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<div id="toptb" class="cl" style="display: none;">
    <div class="wp">
        <div class="z">
            <script type="text/javascript">var _speedMark = new Date();</script>
        </div>
        <div class="y">
            <a id="switchblind" href="javascript:;" onclick="toggleBlind(this)" title="开启辅助访问" class="switchblind">开启辅助访问</a>
        </div>
    </div>
</div>
<ul id="myprompt_menu" class="p_pop" style="display: none;">
    <li><a href="#" id="pm_ntc" style="background-repeat: no-repeat; background-position: 0 50%;"><em
            class="prompt_news_0"></em>消息</a></li>
    <li><a href="#"><em class="prompt_follower_0"></em>新听众</a></li>
    <li class="ignore_noti论坛首页celi"><a href="javascript:;"
                                       onclick="setcookie('ignore_notice', 1);hideMenu('myprompt_menu')"
                                       title="暂不提醒"><em class="ignore_notice"></em></a></li>
</ul>
<div id="qmenu_menu" class="p_pop " style="display: none;">
    <ul class="cl nav"></ul>
</div>
<div class="header-sm">
    <div class="header-main">
        <div class="container-sm clearfix">
            <div class="user">
                <c:choose>
                    <c:when test="${empty sessionScope.user}">
                        <jsp:forward page="login.jsp"></jsp:forward>
                    </c:when>
                    <c:otherwise>
                        <div id="um">
                            <div class="ui-login-toggle">
                                <span class="user-avatar"><img src="${ctx}/avatar/${sessionScope.user.avatar}"/></span>
                                <span class="user-name hide-row">${sessionScope.user.nickname}</span>
                            </div>
                            <div class="ui-login-status">
                                <ul>
                                    <li class="user-primary-info">
                                        <p class="user-avatar-name">
                                            <span class="user-avatar">
                                                <a href="javascript:;"><img
                                                        src="${ctx}/avatar/${sessionScope.user.avatar}"/></a>
                                            </span>
                                            <span class="hide-row">
                                            <a href="javascript:;" target="_self"
                                               class="hide-row">${sessionScope.user.nickname}</a>
                                            </span>
                                        </p>
                                        <p class="user-pipe"></p>
                                    </li>
                                    <li class="user-setting"><a href="${ctx}/admin.jsp">后台管理</a></li>
                                    <li class="user-message"><a href="${ctx}/home.jsp?uid=${sessionScope.user.uid}">个人主页</a>
                                    </li>
                                    <li class="user-setting"><a href="${ctx}/setting.jsp">个人设置</a></li>
                                    <li class="user-logout"><a href="${ctx}/user?userop=logout">退出账号</a></li>
                                </ul>
                            </div>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>
            <ul class="header-menu">
                <li><a target="_self" href="${ctx}/index.jsp">论坛首页</a></li>
                <li><a target="_self" href="${ctx}/forum.jsp">论坛版块</a></li>
            </ul>
        </div>
        <div class="p_pop h_pop" id="mn_userapp_menu" style="display: none"></div>
    </div>
    <div id="forumList" class="header-sub">
        <div class="container-sm">
        </div>
    </div>
    <div class="location">
        <div class="container-sm clearfix">
            <ul id="scbar_type_menu" class="p_pop" style="display: none;">
                <li><a href="javascript:;" rel="forum" class="curtype">帖子</a></li>
                <li><a href="javascript:;" rel="user">用户</a></li>
            </ul>
            <h2><a href="index.jsp">论坛首页</a></h2>
        </div>
    </div>
</div>
<div id="wp" class="wp">
    <div id="pt" class="bm cl">
        <div class="z">
            <a href="${ctx}/index.jsp" class="nvhm" title="首页">论坛</a>
            <em>›</em>
            <a href="#">设置</a>
            <em>›</em>管理
        </div>
    </div>
    <div id="ct" class="ct2_a wp cl">
        <div class="mn">
            <div class="bm bw0">
                <h1 class="mt">管理 </h1>
                <!--don't close the div here-->
                <ul class="tb cl">
                    <li class="a"><a href="javascript:;">用户管理</a></li>
                </ul>
                <table cellspacing="0" cellpadding="0" class="tfm" id="profilelist">

                </table>
            </div>
        </div>
        <div class="appl">
            <div class="tbn">
                <h2 class="mt bbda">管理</h2>
                <ul class="manage">
                    <li class="a"><a href="javascript:;">用户管理</a></li>
                    <li><a href="javascript:;">版块管理</a></li>
                    <li><a href="javascript:;">主题管理</a></li>
                </ul>
            </div>
        </div>
        <div class="forum-list-panel-filter clearfix"
             style="position:static;float:left;margin-top:47px;margin-left:10px;display: none">
            <span>筛选：</span>
            <div class="element-select">
                <div class="element-select-current">
                    <a id="filter_special" href="javascript:;" sid="0">全部版块</a>
                </div>

                <ul>
                    <li class="adminli"><a href="javascript:;" sid="0">全部版块</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
</body>
</html>
