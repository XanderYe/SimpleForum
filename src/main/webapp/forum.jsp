<%@ page language="java" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>论坛板块</title>

    <link rel="stylesheet" type="text/css" href="${ctx}/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/css/utils.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/css/mobile.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/css/jquery-ui.css"/>
    <script src="${ctx}/js/jquery-3.3.1.js"></script>
    <script src="${ctx}/js/forum.js"></script>
</head>

<body>
<c:if test="${empty requestScope.list}">
    <jsp:forward page="section?secop=getallsec"></jsp:forward>
</c:if>

<div class="pop-dialog" id="popQR">
    <div id="popMask" class="dialog-mask" style="display: block;"></div>
    <div class="dialog-box" style="display: block; margin-top: -210px;">
        <div class="dialog-title">
            <h4 class="js-title">可以直接扫描二维码安装</h4>
            <a id="btnClose" class="dialog-close"></a>
        </div>
        <div class="dialog-content" style="height: auto;">
            <div id="dialogQR"></div>
            <p class="js-summary">可直接通过应用商店或手机浏览器下载安装</p>
        </div>
    </div>
</div>

<!-- 兼容旧版 start -->
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<!-- 兼容旧版 end -->

<div id="toptb" class="cl" style="display: none;">
    <div class="wp">
        <div class="z">
            <script type="text/javascript">var _speedMark = new Date();</script>
        </div>
        <div class="y">
            <a id="switchblind" href="javascript:;" onclick="toggleBlind(this)" title="开启辅助访问" class="switchblind">开启辅助访问</a>
        </div>
    </div>
</div>

<div id="qmenu_menu" class="p_pop blk" style="display: none;">
    <div class="ptm pbw hm">
        请 <a href="javascript:;" class="xi2" onclick="lsSubmit()"><strong>登录</strong></a> 后使用快捷导航<br/>没有帐号？<a
            href="${ctx}/member.php?mod=register" class="xi2 xw1">立即注册</a>
    </div>
</div>
<div class="header-sm">
    <div class="header-main">
        <div class="container-sm clearfix">
            <div class="user">
                <c:choose>
                    <c:when test="${empty sessionScope.user}">
                        <form autocomplete="off" id="lsform" class="ui-member">
                            <div class="fastlg cl">
                                <div>
                                    <span class="xi2">
                                        <a id="user-login" href="${ctx}/login.jsp" class="xi2">登录</a>
                                    </span>
                                    <span class="pipe">|</span>
                                    <span class="xi2">
                                        <a id="user-register" href="${ctx}/register.jsp">注册</a>
                                    </span>
                                </div>

                            </div>
                        </form>
                    </c:when>
                    <c:otherwise>
                        <div id="um">
                            <div class="ui-login-toggle">
                                <span class="user-avatar"><img src="${ctx}/avatar/${sessionScope.user.avatar}"/></span>
                                <span class="user-name hide-row">${sessionScope.user.nickname}</span>
                            </div>
                            <div class="ui-login-status">
                                <ul>
                                    <li class="user-primary-info">
                                        <p class="user-avatar-name">
                                    <span class="user-avatar">
                                        <a href="javascript:;"><img src="${ctx}/avatar/${sessionScope.user.avatar}"/></a>
                                    </span> <span class="hide-row">
                                    <a href="javascript:;" target="_self"
                                       class="hide-row">${sessionScope.user.nickname}</a></span>
                                        </p>
                                        <p class="user-pipe"></p>
                                    </li>
                                    <c:if test="${sessionScope.user.permission==99}">
                                        <li class="user-setting"><a href="${ctx}/admin.jsp">后台管理</a></li>
                                    </c:if>
                                    <li class="user-message"><a href="${ctx}/home.jsp?uid=${sessionScope.user.uid}">个人主页</a>
                                    </li>
                                    <li class="user-setting"><a href="${ctx}/setting.jsp">个人设置</a></li>
                                    <li class="user-logout"><a href="${ctx}/user?userop=logout">退出账号</a></li>
                                </ul>
                            </div>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>


            <ul class="header-menu">
                <li><a href="${ctx}/index.jsp">论坛首页</a></li>
                <li><a href="${ctx}/forum.jsp">论坛版块</a></li>

            </ul>

        </div>

        <!-- important { -->
        <div class="p_pop h_pop" id="mn_userapp_menu" style="display: none"></div><!-- } -->


    </div>
    <div id="forumList" class="header-sub">
        <div class="container-sm">


        </div>
    </div>


    <div class="location">
        <div class="container-sm clearfix">
            <div id="scbar" class="cl">
                <form id="scbar_form" method="post" autocomplete="off" onsubmit="searchFocus($('scbar_txt'))"
                      action="search.php?searchsubmit=yes" target="_blank">
                </form>
            </div>
            <ul id="scbar_type_menu" class="p_pop" style="display: none;">
                <li><a href="javascript:;" rel="user">用户</a></li>
            </ul>
            <h2>
                <a href="${ctx}/index.jsp">论坛首页</a>
                <div class="z"></div>
            </h2>
        </div>
    </div>

</div>


<div class="main forums-page clearfix">
    <div class="container-sm">


        <!-- *************** 左侧板块列表页 start **************** -->
        <div class="section">
            <div class="foldableBox content " data-node="category_1">
                <h4>
                    <a href="#" style="">综合区</a>
                    <span title="收起/展开" class="btn-fold"></span>
                </h4>
                <div class="forumList-sm section-content">
                    <ul class="clearfix">
                        <c:forEach items="${requestScope.list}" var="section">
                            <li>
                                <a target="_blank" href="#">
                                    <img src="${ctx}/img/ori.png" alt=""/>
                                </a>
                                <dl>
                                    <dt><h2><a target="_self"
                                               href="${ctx}/post.jsp?sid=${section.sid}"> ${section.sname}</a></h2>
                                    </dt>
                                </dl>
                            </li>
                        </c:forEach>
                    </ul>
                </div>
            </div>
        </div>
        <!-- *************** 左侧板块列表页 end **************** -->


        <!-- *************** 右侧模块 start **************** -->
        <div class="sidebar">


            <!-- *************** 官方应用下载 start **************** -->

            <div class="right-module offical-app">
                <h4>趣味app推荐</h4>
                <ul class="offical-app-list">
                    <li>
                        <img src="https://gss3.bdstatic.com/-Po3dSag_xI4khGkpoWK1HF6hhy/baike/w%3D268%3Bg%3D0/sign=ce79769a30f33a879e6d071cfe677705/34fae6cd7b899e5114f3cfbe48a7d933c8950d1e.jpg"
                             alt="腾讯QQ"/>
                        <dl>
                            <dt><a target="_blank" href="http://shouji.baidu.com/software/9669124.html">腾讯QQ</a></dt>
                            <dd>乐在沟通。</dd>
                        </dl>
                        <a class="qr-trigger" data-href="http://shouji.baidu.com/software/9669124.html"></a>
                    </li>

                    <li>
                        <img src="https://gss2.bdstatic.com/-fo3dSag_xI4khGkpoWK1HF6hhy/baike/w%3D268%3Bg%3D0/sign=b720ea3353ee3d6d22c680cd7b2d0a1f/d0c8a786c9177f3e811405997acf3bc79f3d5606.jpg"
                             alt="哔哩哔哩"/>
                        <dl>
                            <dt><a target="_blank" href="http://shouji.baidu.com/software/23982970.html">哔哩哔哩</a></dt>
                            <dd>国内最大年轻人潮流文化娱乐区。</dd>
                        </dl>
                        <a class="qr-trigger" data-href="http://shouji.baidu.com/software/23982970.html"></a>
                    </li>

                    <li>
                        <img src="https://gss3.bdstatic.com/84oSdTum2Q5BphGlnYG/timg?wapp&quality=80&size=b150_150&subsize=20480&cut_x=0&cut_w=0&cut_y=0&cut_h=0&sec=1369815402&srctrace&di=7d3adc11d2835b51c16d2fca9f1ed93f&wh_rate=null&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fpic%2Fitem%2Fb219ebc4b74543a9d3e26ba217178a82b80114e6.jpg"
                             alt="不思议迷宫"/>
                        <dl>
                            <dt><a target="_blank" href="http://shouji.baidu.com/game/22491754.html">不思议迷宫</a></dt>
                            <dd>绝对不思议。</dd>
                        </dl>
                        <a class="qr-trigger" data-href="http://shouji.baidu.com/game/22491754.html"></a>
                    </li>

                    <li>
                        <img src="http://pic.cr173.com/up/2016-12/2016122617995877.jpg" alt="抖音短视频"/>
                        <dl>
                            <dt><a target="_blank"
                                   href="https://baike.baidu.com/item/%E6%8A%96%E9%9F%B3/20784697?fr=aladdin">抖音短视频</a>
                            </dt>
                            <dd>最火短视频app。</dd>
                        </dl>
                        <a class="qr-trigger"
                           data-href="https://baike.baidu.com/item/%E6%8A%96%E9%9F%B3/20784697?fr=aladdin"></a>
                    </li>

                    <li>
                        <img src="https://gss2.bdstatic.com/-fo3dSag_xI4khGkpoWK1HF6hhy/baike/w%3D268%3Bg%3D0/sign=7281604008e9390156028a3843d733da/1f178a82b9014a90f85b9edda0773912b31bee6d.jpg"
                             alt="追书神器"/>
                        <dl>
                            <dt><a target="_blank"
                                   href="http://www.downza.cn/tags/%E8%BF%BD%E4%B9%A6%E7%A5%9E%E5%99%A8/">追书神器</a></dt>
                            <dd>最多最全的书让你来追。</dd>
                        </dl>
                        <a class="qr-trigger"
                           data-href="http://www.downza.cn/tags/%E8%BF%BD%E4%B9%A6%E7%A5%9E%E5%99%A8/"></a>
                    </li>

                </ul>
            </div>

            <!-- *************** 官方应用下载 end **************** -->

        </div>

        <!-- *************** 右侧模块 end **************** -->

    </div>
</div>


</div>


<div class="back-top"></div>

<div id="mobileTips" class="mobile-tips-download clearfix">


</div>

</body>
</html>
