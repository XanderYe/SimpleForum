<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>个人主页</title>
    <link rel="stylesheet" type="text/css" href="${ctx}/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/css/utils.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/css/mobile.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/css/jquery-ui.css"/>
    <script src="${ctx}/js/jquery-3.3.1.js"></script>
    <script src="${ctx}/js/setting.js"></script>
</head>
<body id="nv_home" class="pg_spacecp" onkeydown="if(event.keyCode==27) return false;">
<c:choose>
    <c:when test="${empty sessionScope.user}">
        <jsp:forward page="login.jsp"></jsp:forward>
    </c:when>
    <c:when test="${empty param.uid}">
        <jsp:forward page="/user?userop=getuser&uid=${sessionScope.user.uid}"></jsp:forward>
    </c:when>
    <c:when test="${empty requestScope.user}">
        <jsp:forward page="/user?userop=getuser&uid=${param.uid}"></jsp:forward>
    </c:when>
</c:choose>
<!-- 兼容旧版 start -->
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<!-- 兼容旧版 end -->

<div id="toptb" class="cl" style="display: none;">
    <div class="wp">
        <div class="z"></div>
        <div class="y">
            <a id="switchblind" href="javascript:;" onclick="toggleBlind(this)" title="开启辅助访问" class="switchblind">开启辅助访问</a>
        </div>
    </div>
</div>

<ul id="myprompt_menu" class="p_pop" style="display: none;">
    <li><a href="#" id="pm_ntc" style="background-repeat: no-repeat; background-position: 0 50%;"><em
            class="prompt_news_0"></em>消息</a></li>

    <li><a href="#"><em class="prompt_follower_0"></em>新听众</a></li>

    <li class="ignore_noticeli"><a href="javascript:;" onclick="setcookie('ignore_notice', 1);hideMenu('myprompt_menu')"
                                   title="暂不提醒"><em class="ignore_notice"></em></a></li>
</ul>
<div id="qmenu_menu" class="p_pop " style="display: none;">
    <ul class="cl nav"></ul>
</div>
<div class="header-sm">
    <div class="header-main">
        <div class="container-sm clearfix">
            <!-- use login status -->
            <div class="user">
                <div id="um">

                    <!-- 头像 -->
                    <div class="ui-login-toggle">
                        <span class="user-avatar"><img src="${ctx}/avatar/${sessionScope.user.avatar}"/></span>
                        <span class="user-name hide-row">${sessionScope.user.username}</span>
                    </div>


                    <div class="ui-login-status">
                        <ul>
                            <li class="user-primary-info">
                                <p class="user-avatar-name">
                                    <span class="user-avatar"><a href="#"><img
                                            src="${ctx}/avatar/${sessionScope.user.avatar}"/></a></span>
                                    <span class="hide-row"><a href="#" target="_self" title="访问我的空间"
                                                              class="hide-row">${sessionScope.user.username}</a></span>
                                </p>
                                <p class="user-pipe">
                                </p>
                            </li>
                            <c:if test="${sessionScope.user.permission==99}">
                                <li class="user-setting"><a href="${ctx}/admin.jsp">后台管理</a></li>
                            </c:if>
                            <li class="user-message"><a href="${ctx}/home.jsp?uid=${sessionScope.user.uid}">个人主页</a></li>
                            <li class="user-setting"><a href="${ctx}/setting.jsp">个人设置</a></li>
                            <li class="user-logout"><a href="${ctx}/user?userop=logout">退出账号</a></li>
                        </ul>


                    </div>
                </div>
            </div>

            <ul class="header-menu">


                <li><a href="${ctx}/index.jsp">论坛首页</a></li>
                <li id="header-fold"><a href="${ctx}/forum.jsp">论坛版块</a></li>
            </ul>

            <!--<div class="user">
            <img src="img/user-portrait.png">
            <h5>HarryZhang</h5>
            </div>-->
        </div>

        <!-- important { -->
        <div class="p_pop h_pop" id="mn_userapp_menu" style="display: none"></div><!-- } -->


    </div>
    <div id="forumList" class="header-sub">
        <div class="container-sm">


        </div>
    </div>
    <div class="location">
        <div class="container-sm clearfix">
            <div id="scbar" class="cl">
                <form id="scbar_form" method="post" autocomplete="off" onsubmit="searchFocus($('scbar_txt'))"
                      action="search.php?searchsubmit=yes" target="_self">
                    <input type="hidden" name="mod" id="scbar_mod" value="search"/>
                    <input type="hidden" name="formhash" value="6048d1bc"/>
                    <input type="hidden" name="srchtype" value="title"/>
                    <input type="hidden" name="srhfid" value="0"/>
                    <input type="hidden" name="srhlocality" value="home::spacecp"/>
                    <table cellspacing="0" cellpadding="0">
                    </table>
                </form>
            </div>
            <ul id="scbar_type_menu" class="p_pop" style="display: none;">
                <li><a href="javascript:;" rel="forum" class="curtype">帖子</a></li>
                <li><a href="javascript:;" rel="user">用户</a></li>
            </ul>
            <h2>
                <a href="${ctx}/index.jsp">论坛首页</a>
            </h2>
        </div>
    </div>
</div>
<div id="wp" class="wp">
    <div id="pt" class="bm cl">
        <div class="z">
            <a href="${ctx}/index.jsp" class="nvhm" title="首页">论坛</a> <em>›</em>
            <a href="#">${sessionScope.user.nickname}</a> <em>›</em>
            个人资料
        </div>
    </div>
    <style id="diy_style" type="text/css"></style>
    <div class="wp">
        <!--[diy=diy1]-->
        <div id="diy1" class="area"></div><!--[/diy]-->
    </div>
    <div id="uhd">
        <div class="h cl">
            <div class="icn avt"><a href="${ctx}/home.jsp"><img src="${ctx}//avatar/${requestScope.user.avatar}"></a></div>
            <h2 class="mt">
                ${requestScope.user.username}</h2>
            <p>
                <a href="${ctx}/home.jsp?uid=${requestScope.user.uid}" class="xg1">个人主页</a>
            </p>
        </div>

        <ul class="tb cl" style="padding-left: 75px;">
            <li class="a"><a href="javascript:;">个人资料</a></li>
        </ul>
    </div>
    <div id="ct" class="ct1 wp cl">
        <div class="mn">
            <!--[diy=diycontenttop]-->
            <div id="diycontenttop" class="area"></div><!--[/diy]-->
            <div class="bm bw0">
                <div class="bm_c">
                    <div class="bm_c u_profile">

                        <div class="pbm mbm bbda cl">
                            <h2 class="mbn">
                                ${requestScope.user.username}<span class="xw0">(UID: ${requestScope.user.uid})</span>
                            </h2>
                            <ul class="pf_l cl pbm mbm">

                            </ul>
                            <ul>
                            </ul>
                            <ul class="cl bbda pbm mbm">
                                <li>
                                    <span>昵称：${requestScope.user.nickname}</span><br>
                                    <span>email：${requestScope.user.email}</span><br>
                                    <span>性别:${requestScope.user.gender}</span><br>
                                    <span>生日:${requestScope.user.birthday}</span><br>
                                    <span>住宅地址:${requestScope.user.address }</span><br>
                                    <span>积分：${requestScope.user.score}</span>
                                </li>
                            </ul>
                            <ul class="pf_l cl"></ul>
                        </div>
                        <div class="pbm mbm bbda cl">

                        </div>

                    </div>
                    <!--[diy=diycontentbottom]-->
                    <div id="diycontentbottom" class="area"></div><!--[/diy]--></div>
            </div>
        </div>
    </div>


</div>
</div>
</div>


</body>
</html>
