
$(function () {
    newPopover();
    var flag = false;
    $("#login").click(function () {
        location.href = "login.jsp";
    });

    $("input[name=username]").blur(function () {
        var username = $(this).val();
        if (username == "") {
            $(".checknull").popover("show");
        } else {
            $.ajax({
                type: "POST",
                url: "user?userop=check",
                data: {"username": username},
                success: function (data) {
                    flag = data;
                    if (data == "true") {
                        $(".usercheck").popover("show");
                    } else {
                        $(".usercheck").popover("hide");
                    }
                },
                error: function () {
                    console.log("error");
                }
            });
        }
    });
    $("input[name=username]").keyup(function () {
        if ($(this).val().length > 0) {
            $(".checknull").popover("hide");
        }
    });

    $("input[name=nickname]").blur(function () {
        var nickname = $(this).val();
        if (nickname == "") {
            $(".nickcheck").popover("show");
        }
    });
    $("input[name=nickname]").keyup(function () {
        if ($(this).val().length > 0) {
            $(".nickcheck").popover("hide");
        }
    });

    $("input[name=password]").blur(function () {
        var password = $(this).val();
        if (password == "") {
            $(".pwdcheck1").popover("show");
        }
    });
    $("input[name=password]").keyup(function () {
        if ($(this).val().length > 0) {
            $(".pwdcheck1").popover("hide");
        }
    });

    $("input[name=password2]").blur(function () {
        var password2 = $(this).val();
        var password1 = $("input[name=password]").val();
        if (password1 == "" || password1 != password2) {
            $(".pwdcheck2").popover("show");
        }
    });
    $("input[name=password2]").keyup(function () {
        if ($(this).val().length > 0) {
            $(".pwdcheck2").popover("hide");
        }
    });
    $("#register").click(function () {
        var username = $("input[name=username]").val();
        var nickname = $("input[name=nickname]").val();
        var password1 = $("input[name=password]").val();
        var password2 = $("input[name=password2]").val();
        if (flag == "true") {
            alert("用户名已存在！");
            return false;
        }
        if (username == "" || nickname == "" || password1 == "" || password2 == "") {
            alert("请检查各项不为空！");
            return false;
        }
        if (password1.length <= 6) {
            console.log(password1);
            alert("密码过于简单！");
            return false;
        }
        if (password1 != password2) {
            alert("两次密码不一致！");
            return false;
        }
        return true;
    });
});

function newPopover() {
    $("input[name=username]").popover({
        trigger: "focus",
        placement: "bottom",
        content: "用户名由20位以内的英文与数字组成"
    });
    $("input[name=nickname]").popover({
        trigger: "focus",
        placement: "bottom",
        content: "昵称由20位以内的中、英文与数字组成"
    });
    $("input[type=password]").popover({
        trigger: "focus",
        placement: "bottom",
        content: "密码长度 6~16 位，数字、字母和符号至少包含两种"
    });
    $(".usercheck").popover({
        trigger: "manual",
        placement: "right",
        content: "用户名已存在"
    });
    $(".checknull").popover({
        trigger: "manual",
        placement: "right",
        content: "用户名不为空"
    });
    $(".nickcheck").popover({
        trigger: "manual",
        placement: "right",
        content: "昵称不为空"
    });
    $(".pwdcheck1").popover({
        trigger: "manual",
        placement: "right",
        content: "密码不为空"
    });
    $(".pwdcheck2").popover({
        trigger: "manual",
        placement: "right",
        content: "两次密码不一致"
    });
}