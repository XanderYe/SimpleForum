$(function () {
    $(".user").mousemove(function () {
        $(".ui-login-status").css("display", "block");
    });
    $(".user").mouseout(function () {
        $(".ui-login-status").css("display", "none");
    });
    $(".element-select").click(function () {
        if ($(this).hasClass("on")) {
            $(this).removeClass("on");
        } else {
            $(this).addClass("on");
        }
    });
    $("button[name=profilesubmitbtn]").click(function () {
        var nickname = $("#nickname").val();
        var email = $("#email").val();
        var birthday = $("#birthday").val();
        var address = $("#nickname").val();
        if (nickname == "") {
            alert("昵称不能为空！");
            return false;
        }
        if (email == "") {
            alert("邮箱不能为空！");
            return false;
        }
        if (birthday == "") {
            alert("生日不能为空！");
            return false;
        }
        if (address == "") {
            alert("住址不能为空！");
            return false;
        }
        return true;
    });
    $("#birthday").datepicker({
        "dateFormat": "yy-mm-dd"
    });
    $(".verify").click(function () {
        var username = $(".uname").text();
        var email = $("#email").val();
        $.ajax({
            type: "POST",
            url: "email?emailop=send",
            data: {"username": username, "email": email},
            dataType: "JSON",
            success: function (res) {
                if (res == true) {
                    alert("邮件发送成功！如没有请注意垃圾箱");
                } else {
                    alert("邮件发送失败！");
                }
            },
            error: function () {

            }
        });
    });
    $(".manage li").click(function () {
        $(".manage li").removeClass("a");
        $(this).addClass("a");
    });
});