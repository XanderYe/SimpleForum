$(function () {
    var urldel = "user?userop=delete";
    getUsers();
    $(".user").mousemove(function () {
        $(".ui-login-status").css("display", "block");
    });
    $(".user").mouseout(function () {
        $(".ui-login-status").css("display", "none");
    });
    $(".element-select").click(function () {
        if ($(this).hasClass("on")) {
            $(this).removeClass("on");
        } else {
            $(this).addClass("on");
        }
    });
    $(".manage li").click(function () {
        $(".manage li").removeClass("a");
        $(this).addClass("a");
        var operation = $(this).children(".a a").text();
        $(".tb .a a").html(operation);
        if (operation == "用户管理") {
            $(".forum-list-panel-filter").css("display", "none");
            urldel = "user?userop=delete";
            getUsers();
        } else if (operation == "版块管理") {
            $(".forum-list-panel-filter").css("display", "none");
            urldel = "section?secop=delete";
            getSections();
        } else if (operation == "主题管理") {
            $(".forum-list-panel-filter").css("display", "block");
            SectionSelete();
            urldel = "topic?topicop=delete";
            getTopics(0);
        }
    });
    $(".element-select").on("click", ".adminli", function () {
        var sid = $(this).children("a").attr("sid");
        $("#filter_special").text($(this).children("a").text());
        $("#filter_special").attr("sid", sid);
        getTopics(sid);
    });
    $("#profilelist").on("click", ".save", function () {
        var operation = $(".a a").eq(0).text();
        if (operation == "用户管理") {
            var uid = $(this).parent("td").parent("tr").find("input[name=id]").val();
            var uname = $(this).parent("td").parent("tr").find("input[name=username]").val();
            var unname = $(this).parent("td").parent("tr").find("input[name=nickname]").val();
            var upwd = $(this).parent("td").parent("tr").find("input[name=password]").val();
            var email = $(this).parent("td").parent("tr").find("input[name=email]").val();
            saveUser(uid, uname, unname, upwd, email);
        } else if (operation == "版块管理") {
            var sid = $(this).parent("td").parent("tr").find("input[name=id]").val();
            var sname = $(this).parent("td").parent("tr").find("input[name=sname]").val();
            saveSection(sid, sname);
        } else if (operation == "主题管理") {
            var tid = $(this).parent("td").parent("tr").find("input[name=id]").val();
            var sid = $(this).parent("td").parent("tr").find("input[name=sid]").val();
            var sname = $(this).parent("td").parent("tr").find("input[name=sname]").val();
            saveTopic(tid, sid, sname);
        }
    });
    $("#profilelist").on("click", ".delete", function () {
        if (clickdel()) {
            var uid = $(this).parent("td").parent("tr").find("input[name=id]").val();
            var tr = $(this).parent("td").parent("tr");
            del(urldel, uid, tr);
        }
    });
});

function getUsers() {
    $("#profilelist").children().remove();
    var item;
    item = "<tr>\n" +
        "<td>UID</td>\n" +
        "<td>用户名</td>\n" +
        " <td>昵称</td>\n" +
        "<td>密码</td>\n" +
        "<td>邮箱</td>\n" +
        "<td>操作</td>\n" +
        "</tr>";
    $("#profilelist").append(item);
    $.ajax({
        type: "POST",
        url: "user?userop=getall",
        data: {},
        dataType: "JSON",
        success: function (data) {
            $.each(data, function (index, value) {
                item = "<tr>" +
                    "<td><input type='text' size='2px' name='id' value=" + value.uid + "></td>" +
                    "<td><input type='text' size='10px' name='username' value=" + value.username + "></td>" +
                    " <td><input type='text' size='10px' name='nickname' value=" + value.nickname + "></td>" +
                    "<td><input type='text' size='10px' name='password' value=" + value.password + "></td>";
                if (value.hasOwnProperty("email")) {
                    item += "<td><input type='text' size='10px' name='email' value=" + value.email + "></td>\n";
                } else {
                    item += "<td><input type='text' size='10px' name='email' value=''></td>\n";
                }
                item += "<td><button type='button' name='profilesubmitbtn' id='profilesubmitbtn' value='true' class='pn pnc save'><strong>保存</strong></button>" +
                    "<button type='button' name='profilesubmitbtn' id='profilesubmitbtn' value='true' class='pn pnc delete'><strong>删除</strong></button>" +
                    "</td></tr>";
                $("#profilelist").append(item);
            });
        },
        error: function () {
            console.log("error");
        }
    });
}

function getSections() {
    $("#profilelist").children().remove();
    var item;
    item = "<tr>\n" +
        "<td>SID</td>\n" +
        "<td>版块名</td>\n" +
        "<td>操作</td>\n" +
        "</tr>";
    $("#profilelist").append(item);
    $.ajax({
        type: "POST",
        url: "section?secop=getall",
        data: {},
        dataType: "JSON",
        async: false,
        success: function (data) {
            $.each(data, function (index, value) {
                item = "<tr>" +
                    "<td><input type='text' size='2px' name='id' value=" + value.sid + "></td>" +
                    "<td><input type='text' size='10px' name='sname' value=" + value.sname + "></td>" +
                    "<td><button type='button' name='profilesubmitbtn' id='profilesubmitbtn' value='true' class='pn pnc save'><strong>保存</strong></button>" +
                    "<button type='button' name='profilesubmitbtn' id='profilesubmitbtn' value='true' class='pn pnc delete'><strong>删除</strong></button>" +
                    "</td></tr>";
                $("#profilelist").append(item);
            });
        },
        error: function () {
            console.log("error");
        }
    });
    item = "<tr>" +
        "<td><input type='text' size='2px' name='id' disabled='disabled'></td>" +
        "<td><input type='text' size='10px' name='sname'></td>" +
        "<td><button type='button' name='profilesubmitbtn' id='profilesubmitbtn' value='true' class='pn pnc save'><strong>添加</strong></button>" +
        "<button type='button' name='profilesubmitbtn' id='profilesubmitbtn' value='true' class='pn pnc delete'><strong>删除</strong></button>" +
        "</td>></tr>";
    $("#profilelist").append(item);
}

function getTopics(sid) {
    $("#profilelist").children().remove();
    var item;
    item = "<tr>\n" +
        "<td>TID</td>\n" +
        "<td>SID</td>\n" +
        "<td>主题名</td>\n" +
        "<td>操作</td>\n" +
        "</tr>";
    $("#profilelist").append(item);
    $.ajax({
        type: "POST",
        url: "topic?topicop=getall",
        data: {"sid": sid},
        dataType: "JSON",
        async: false,
        success: function (data) {
            $.each(data, function (index, value) {
                item = "<tr>" +
                    "<td><input type='text' size='2px' name='id' value=" + value.tid + "></td>" +
                    "<td><input type='text' size='2px' name='sid' value=" + value.sid + "></td>" +
                    "<td><input type='text' size='10px' name='sname' value=" + value.tname + "></td>" +
                    "<td><button type='button' name='profilesubmitbtn' id='profilesubmitbtn' value='true' class='pn pnc save'><strong>保存</strong></button>" +
                    "<button type='button' name='profilesubmitbtn' id='profilesubmitbtn' value='true' class='pn pnc delete'><strong>删除</strong></button>" +
                    "</td></tr>";
                $("#profilelist").append(item);
            });
        },
        error: function () {
            console.log("error");
        }
    });
    var sid = $("#filter_special").attr("sid");
    if (sid == 0) sid = "";
    item = "<tr>" +
        "<td><input type='text' size='2px' name='id' disabled='disabled'></td>" +
        "<td><input type='text' size='2px' name='sid' value='" + sid + "'></td>" +
        "<td><input type='text' size='10px' name='sname'></td>" +
        "<td><button type='button' name='profilesubmitbtn' id='profilesubmitbtn' value='true' class='pn pnc save'><strong>添加</strong></button>" +
        "<button type='button' name='profilesubmitbtn' id='profilesubmitbtn' value='true' class='pn pnc delete'><strong>删除</strong></button>" +
        "</td>></tr>";
    $("#profilelist").append(item);
}

function SectionSelete() {
    var ul = $(".element-select ul");
    $(".adminli").eq(0).nextAll().remove();
    $.ajax({
        type: "POST",
        url: "section?secop=getall",
        data: {},
        dataType: "JSON",
        async: false,
        success: function (data) {
            $.each(data, function (index, value) {
                item = "<li class='adminli'><a href='javascript:;' sid=" + value.sid + ">" + value.sname + "</a></li>";
                ul.append(item);
            });
        },
        error: function () {
            console.log("error");
        }
    });
    item = "<tr>" +
        "<td><input type='text' size='2px' name='id' disabled='disabled'></td>" +
        "<td><input type='text' size='10px' name='sname'></td>" +
        "<td><button type='button' name='profilesubmitbtn' id='profilesubmitbtn' value='true' class='pn pnc save'><strong>添加</strong></button>" +
        "<button type='button' name='profilesubmitbtn' id='profilesubmitbtn' value='true' class='pn pnc delete'><strong>删除</strong></button>" +
        "</td>></tr>";
    $("#profilelist").append(item);
}

function saveUser(uid, uname, unname, upwd, email) {
    $.ajax({
        type: "POST",
        url: "user?userop=update",
        data: {"uid": uid, "username": uname, "nickname": unname, "password": upwd, "email": email},
        success: function (data) {
            if (data > 0) {
                alert('修改成功！');
            } else {
                alert('修改失败！');
            }
            getUsers();
        },
        error: function () {
            console.log("error");
        }
    });
}

function saveSection(sid, sname) {
    var url;
    console.log(sid);
    if (sid == "") {
        url = "section?secop=insert";
    } else {
        url = "section?secop=update";
    }
    $.ajax({
        type: "POST",
        url: url,
        data: {"sid": sid, "sname": sname},
        success: function (data) {
            if (data > 0) {
                alert('修改/添加成功！');
            } else {
                alert('没有改动！');
            }
            getSections();
        },
        error: function () {
            console.log("error");
        }
    });

}

function saveTopic(tid, sid, tname) {
    var url;
    if (tid == "") {
        url = "topic?topicop=insert";
    } else {
        url = "topic?topicop=update";
    }
    $.ajax({
        type: "POST",
        url: url,
        data: {"tid": tid, "sid": sid, "tname": tname},
        success: function (data) {
            if (data > 0) {
                alert('修改/添加成功！');
            } else {
                alert('没有改动！');
            }
            getTopics($("#filter_special").attr("sid"));
        },
        error: function () {
            console.log("error");
        }
    });

}

function del(url, id, tr) {
    $.ajax({
        type: "POST",
        url: url,
        data: {"id": id},
        success: function (data) {
            if (data > 0) {
                alert('删除成功！');
                tr.remove();
            } else {
                alert('删除失败！');
            }
        },
        error: function () {
            console.log("error");
        }
    });
}

function clickdel() {
    return confirm("确认删除吗？");
}