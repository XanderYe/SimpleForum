$(function () {
    var sid=$(".sid").attr("sid");
    getTopics(sid);

    //发帖聚焦帖子标题栏
    $("#filter-post-btn").click(function () {
        $("#subject").focus();
        $('html,body').animate({ scrollTop: $(document).height() }, 500); return false;

    });
    $(".element-select").click(function () {
        if ($(this).hasClass("on")) {
            $(this).removeClass("on");
        } else {
            $(this).addClass("on");
        }
    });
    $(".element-select").on("click", ".topic li", function () {
        $(this).parent("ul").prev(".element-select-current").children("a").text($(this).children("a").text());
    });

    //发表帖子
    $("#fastpostsubmit").click(function () {
        var sid=$(".sid").attr("sid");
        var topic=$("#topic").text();
        var topics=$(".topic a");
        var tid;
        for(var i=0;i<topics.length;i++){
            if(topics.eq(i).text()==topic){
                tid=topics.eq(i).attr("tid");
            }
        }
        var title=$("#subject").val();
        var text=editor.html();
        var uid=$(".uid").attr("uid");
        console.log(uid);
        var code=$("#seccodeverify").val();
        var date=new Date().Format("yyyy-MM-dd HH:mm:ss");
        var flag=true;
        var count1;
        var count2;
        if(uid==""){
            alert("请先登录！");
            location.href="login.jsp";
        }else if(topic=="选择主题分类"){
            flag=false;
            alert("请选择主题！");
        }else if(title==""){
            flag=false;
            alert("标题不为空！");
        }else if(text==""){
            flag=false;
            alert("内容不为空！");
        }else if(code==""){
            flag=false;
            alert("验证码不为空！");
        }
        if(flag){
            count1=addPost(sid,tid,title,text,uid,date,code);
        }
    });

    //更换验证码
    $("#change").click(function () {
        var img=$("#change img");
        img.prop("src","authImage?date="+new Date());
    });
    
    $(".pg").on("click",".page",function () {
        
    });
});

//生成kindeditor
var editor;
KindEditor.ready(function(K) {
        editor = K.create('textarea[name="content"]', {
        cssPath : 'css/themes/default/default.css',
        uploadJson : 'upload_json.jsp',
        fileManagerJson : 'file_manager_json.jsp',
        allowFileManager : true,
        width : '720px',
        height : '245px',
        resizeType: '0',
        afterCreate : function() {
            var self = this;
            K.ctrl(document, 13, function() {
                self.sync();
                document.forms['example'].submit();
            });
            K.ctrl(self.edit.doc, 13, function() {
                self.sync();
                document.forms['example'].submit();
            });
        }
    });
    prettyPrint();
});

//方法 根据sid显示主题
function getTopics(sid) {
    $(".on").nextAll().remove();
    $(".topic").children().remove();
    var item;
    $.ajax({
        type: "POST",
        url: "topic?topicop=getall",
        data: {"sid": sid},
        dataType: "JSON",
        async: false,
        success: function (data) {
            $.each(data, function (index, value) {
                item="<li><a href='post.jsp?sid="+sid+"&tid="+value.tid+"'>"+value.tname+"</a></li>";
                $(".forum-list-panel-category").append(item);
                item="<li><a tid='"+value.tid+"' href='javascript:;'>"+value.tname+"</a></li>";
                $(".topic").append(item);
            });
        },
        error: function () {
            console.log("error");
        }
    });
}


//方法 添加帖子
function addPost(sid,tid,title,text,uid,createtime,code) {
    $.ajax({
        type: "POST",
        url: "post?postop=addpost",
        data: {"sid": sid,"tid":tid,"title":title,"content":text,"uid":uid,"createtime":createtime,"code":code},
        async: false,
        success: function (data) {
            if(data=="verfail"){
                alert("验证码错误！");
                var img=$("#change img");
                img.prop("src","/authImage?date="+new Date());
            }else if(data=="false"){
                alert("发表失败！");
            }else if(data=="true"){
                alert("发表成功！");
                location.reload();
            }
        },
        error: function () {
            console.log("error");
        }
    });
}

//日期格式化
Date.prototype.Format = function (fmt) { //author: meizz
    var o = {
        "M+": this.getMonth() + 1, //月份
        "d+": this.getDate(), //日
        "H+": this.getHours(), //小时
        "m+": this.getMinutes(), //分
        "s+": this.getSeconds(), //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}