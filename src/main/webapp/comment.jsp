<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/5/23
  Time: 10:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<%
    String htmlData = request.getParameter("content") != null ? request.getParameter("content") : "";
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>${requestScope.post.ptitle}　论坛</title>
    <link rel="stylesheet" type="text/css" href="${ctx}/css/viewthread.css">
    <link rel="stylesheet" type="text/css" href="${ctx}/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/css/utils.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/css/mobile.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/css/jquery-ui.css"/>
    <link rel="stylesheet" href="${ctx}/js/themes/default/default.css"/>
    <link rel="stylesheet" href="${ctx}/css/prettify.css"/>
    <script src="${ctx}/js/jquery-3.3.1.js"></script>
    <script charset="utf-8" src="${ctx}/js/kindeditor-all.js"></script>
    <script charset="utf-8" src="${ctx}/js/zh-CN.js"></script>
    <script charset="utf-8" src="${ctx}/js/prettify.js"></script>
    <script src="${ctx}/js/forum.js"></script>
    <script src="${ctx}/js/comment.js"></script>
    <style type="text/css">
        #authorposton5897387 {
            font-size: 13px;
        }

        #postmessage_5897387 p {
            line-height: 30px;
        }
        .pnpost label,.y{
            font-size:12px;
        }

    </style>
</head>
<body id="nv_forum" class="pg_viewthread" onkeydown="if(event.keyCode==27) return false;">
<c:choose>
    <c:when test="${empty param.page}">
        <c:set var="page" value="1"></c:set>
    </c:when>
    <c:otherwise>
        <c:set var="page" value="${param.page}"></c:set>
    </c:otherwise>
</c:choose>
<c:if test="${empty param.pid}">
    <jsp:forward page="forum.jsp"></jsp:forward>
</c:if>
<c:if test="${empty requestScope.post}">
    <jsp:forward page="/comment?comop=getcomments&pid=${param.pid}&page=${page}"></jsp:forward>
</c:if>

<c:set var="post" value="${requestScope.post}"></c:set>
<c:set var="comments" value="${requestScope.comments}"></c:set>
<c:set var="count" value="${requestScope.count}"></c:set>
<c:set var="maxpage" value="${requestScope.maxpage}"></c:set>
<div class="pid" pid="${post.pid}"></div>
<div class="uid" uid="${sessionScope.user.uid}"></div>
<!-- 兼容旧版 start -->
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<!-- 兼容旧版 end -->

<div id="toptb" class="cl" style="display: none;">
    <div class="wp">
        <div class="z">
            <script type="text/javascript">var _speedMark = new Date();</script>
        </div>
        <div class="y">
            <a id="switchblind" href="javascript:;" onclick="toggleBlind(this)" title="开启辅助访问" class="switchblind">开启辅助访问</a>
        </div>
    </div>
</div>

<ul id="myprompt_menu" class="p_pop" style="display: none;">
    <li><a href="http://bbs.smartisan.com/home.php?mod=space&amp;do=pm" id="pm_ntc"
           style="background-repeat: no-repeat; background-position: 0 50%;"><em class="prompt_news_0"></em>消息</a></li>

    <li><a href="http://bbs.smartisan.com/home.php?mod=follow&amp;do=follower"><em
            class="prompt_follower_0"></em>新听众</a></li>

    <li class="ignore_noticeli"><a href="javascript:;" onclick="setcookie('ignore_notice', 1);hideMenu('myprompt_menu')"
                                   title="暂不提醒"><em class="ignore_notice"></em></a></li>
</ul>
<div id="qmenu_menu" class="p_pop " style="display: none;">
    <ul class="cl nav"></ul>
</div>
<div class="header-sm">
    <div class="header-main">
        <div class="container-sm clearfix">

            <div class="user">
                <c:choose>
                    <c:when test="${empty sessionScope.user}">
                        <form autocomplete="off" id="lsform" class="ui-member">
                            <div class="fastlg cl">
                                <div>
                                    <span class="xi2">
                                        <a id="user-login" href="${ctx}/login.jsp" class="xi2">登录</a>
                                    </span>
                                    <span class="pipe">|</span>
                                    <span class="xi2">
                                        <a id="user-register" href="${ctx}/register.jsp">注册</a>
                                    </span>
                                </div>

                            </div>
                        </form>
                    </c:when>
                    <c:otherwise>
                        <div id="um">
                            <div class="ui-login-toggle">
                                <span class="user-avatar"><img src="${ctx}/avatar/${sessionScope.user.avatar}"/></span>
                                <span class="user-name hide-row">${sessionScope.user.nickname}</span>
                            </div>
                            <div class="ui-login-status">
                                <ul>
                                    <li class="user-primary-info">
                                        <p class="user-avatar-name">
                                    <span class="user-avatar">
                                        <a href="javascript:;"><img src="${ctx}/avatar/${sessionScope.user.avatar}"/></a>
                                    </span> <span class="hide-row">
                                    <a href="javascript:;" target="_self"
                                       class="hide-row">${sessionScope.user.nickname}</a></span>
                                        </p>
                                        <p class="user-pipe"></p>
                                    </li>
                                    <c:if test="${sessionScope.user.permission==99}">
                                        <li class="user-setting"><a href="${ctx}/admin.jsp">后台管理</a></li>
                                    </c:if>
                                    <li class="user-message"><a href="${ctx}/home.jsp?uid=${sessionScope.user.uid}">个人主页</a>
                                    </li>
                                    <li class="user-setting"><a href="${ctx}/setting.jsp">个人设置</a></li>
                                    <li class="user-logout"><a href="${ctx}/user?userop=logout">退出账号</a></li>
                                </ul>
                            </div>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>

            <ul class="header-menu">
                <li><a target="_self" href="${ctx}/index.jsp">论坛首页</a></li>
                <li><a target="_self" href="${ctx}/forum.jsp">论坛版块</a></li>
            </ul>

            <!--<div class="user">
            <img src="img/user-portrait.png">
            <h5>HarryZhang</h5>
            </div>-->
        </div>

        <!-- important { -->
        <div class="p_pop h_pop" id="mn_userapp_menu" style="display: none"></div><!-- } -->


    </div>
    <div class="location">
        <div class="container-sm clearfix">
            <div id="scbar" class="cl">

            </div>
            <ul id="scbar_type_menu" class="p_pop" style="display: none;">
                <li><a href="javascript:;" rel="curforum" fid="167">本版</a></li>
                <li><a href="javascript:;" rel="forum" class="curtype">帖子</a></li>
                <li><a href="javascript:;" rel="user">用户</a></li>
            </ul>
            <h2>
                <a href="${ctx}/index.jsp">论坛首页</a>
                <em>&rsaquo;</em>
                <a href="${ctx}/forum.jsp">综合区</a>
                <em>&rsaquo;</em>
                <a href="${ctx}/post.jsp?sid=${post.sid}">${requestScope.sname}</a>
                <em>›</em>
                <a href="${ctx}/comment.jsp?pid=${post.pid}" id="thread_subject">${post.ptitle}</a>
            </h2>
        </div>
    </div>
</div>


<div id="wp" class="wp">

    <style id="diy_style" type="text/css"></style>
    <!--[diy=diynavtop]-->
    <div id="diynavtop" class="area"></div><!--[/diy]-->

    <style id="diy_style" type="text/css"></style>
    <div class="wp">
        <!--[diy=diy1]-->
        <div id="diy1" class="area"></div><!--[/diy]-->
    </div>

    <div id="ct" class="wp cl">
        <div id="pgt" class="pgs mbm cl ">
            <div class="pgt"></div>
            <span class="y pgb"><a href="${ctx}/post.jsp?sid=${post.sid}">返回列表</a></span>
            <a class="btn-default " id="newspecial" href="${ctx}/post.jsp?sid=${post.sid}" title="发新帖">发帖</a>
            <a class="btn-default " id="post_reply" href="javascript:;" title="回复">回复</a>
        </div>

        <c:forEach items="${comments}" var="comment">
            <c:choose>
                <c:when test="${comment.cfloor==1}">
                    <div id="postlist" class="pl bm">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="pls ptn pbn">

                                    <div class="hm ptn">

                                        <span class="xg1">回复:</span><span class="xi1">${count}</span>
                                    </div>

                                </td>
                                <td class="plc ptm pbn vwthd">


                                    <div class="y">

                                    </div>
                                    <h1 class="ts">
                                        <a href="#">[${post.topic.tname}]</a>
                                        <span id="thread_subject">${post.ptitle}</span>
                                    </h1>
                                    <span class="xg1">

                    </span>
                                </td>
                            </tr>
                        </table>


                        <table cellspacing="0" cellpadding="0" class="ad">
                            <tr>
                                <td class="pls">
                                </td>
                                <td class="plc">
                                </td>
                            </tr>
                        </table>
                        <div id="post_5897387">
                            <table id="pid5897387" class="plhin" summary="pid5897387" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="pls" rowspan="2">
                                        <div id="favatar5897387" class="pls favatar">
                                            <div class="pi">
                                                <div class="authi"><a href="${ctx}/home.jsp?uid=${comment.user.uid}">${comment.user.nickname}</a>
                                                </div>
                                            </div>
                                            <div class="p_pop blk bui card_gender_0" id="userinfo5897387"
                                                 style="display: none; margin-top: -11px;">
                                                <div class="m z">
                                                    <div id="userinfo5897387_ma"></div>
                                                </div>
                                                <div class="i y">
                                                    <div>

                                                    </div>
                                                    <dl class="cl">

                                                    </dl>
                                                    <div class="imicn">

                                                    </div>
                                                    <div id="avatarfeed"><span id="threadsortswait"></span></div>
                                                </div>
                                            </div>
                                            <div class="hole-avatar-box">

                                                <div class="avatar" onmouseover="showauthor(this, 'userinfo5897387')">
                                                        <%--头像--%>
                                                    <a href="#" class="avtm" target="_blank"><img
                                                            src="${ctx}/avatar/${comment.user.avatar}"/></a>

                                                </div>
                                            </div>
                                    <td class="plc">
                                        <div class="pi">

                                            <div id="fj" class="y">


                                            </div>
                                            <strong>
                                                <a href="#" id="postnum5897387"
                                                   onclick="setCopy(this.href, '帖子地址复制成功');return false;">
                                                    <em>${comment.cfloor}</em>楼</a>
                                            </strong>
                                            <div class="pti">
                                                <div class="pdbt">
                                                </div>
                                                <div class="authi">
                                                    <em id="authorposton5897387">发表时间　${comment.ctime}</em>
                                                    <span class="pipe"></span>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="pct">
                                            <style type="text/css">.pcb {
                                                margin-right: 0
                                            }</style>
                                            <div class="pcb">
                                                <div class="t_fsz">
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="t_f js-thread-content" id="postmessage_5897387">
                                                                    ${comment.ccontent}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div id="comment_5897387" class="cm">
                                            </div>
                                            <div id="post_rate_div_5897387"></div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="plc plm">
                                        <div id="p_btn" class="mtw mbm hm cl">


                                        </div>
                                    </td>
                                </tr>
                                <tr id="_postposition5897387"></tr>
                                <tr>
                                    <td class="pls"></td>
                                    <td class="plc" style="overflow:visible;">
                                        <div class="po hin">
                                            <div class="pob cl">
                                                <em>
                                                    <a class="fastre hostre" href="javascript:;">回复</a>
                                                </em>

                                                <p>
                                                    <a href="javascript:;">举报</a>
                                                </p>

                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="ad">
                                    <td class="pls">
                                    </td>
                                    <td class="plc">
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div id="postlistreply" class="pl">
                            <div id="post_new" class="viewthread_table" style="display: none"></div>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <div id="postlist" class="pl bm">
                        <table cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="pls ptn pbn">

                                    <div class="hm ptn">

                                        <span class="xg1"></span> <span class="xi1"></span>
                                    </div>

                                </td>
                                <td class="plc ptm pbn vwthd">


                                    <div class="y">

                                    </div>
                                    <h1 class="ts">
                                        <a href="#"></a>
                                        <span id="thread_subject"></span>
                                    </h1>
                                    <span class="xg1">

                    </span>
                                </td>
                            </tr>
                        </table>


                        <table cellspacing="0" cellpadding="0" class="ad">
                            <tr>
                                <td class="pls">
                                </td>
                                <td class="plc">
                                </td>
                            </tr>
                        </table>
                        <div id="post_5897387">
                            <table id="pid5897387" class="plhin" summary="pid5897387" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="pls" rowspan="2">
                                        <div id="favatar5897387" class="pls favatar">
                                            <div class="pi">
                                                <div class="authi"><a href="${ctx}/home.jsp?uid=${comment.user.uid}">${comment.user.nickname}</a>
                                                </div>
                                            </div>
                                            <div class="p_pop blk bui card_gender_0" id="userinfo5897387"
                                                 style="display: none; margin-top: -11px;">
                                                <div class="m z">
                                                    <div id="userinfo5897387_ma"></div>
                                                </div>
                                                <div class="i y">
                                                    <div>

                                                    </div>
                                                    <dl class="cl">

                                                    </dl>
                                                    <div class="imicn">

                                                    </div>
                                                    <div id="avatarfeed"><span id="threadsortswait"></span></div>
                                                </div>
                                            </div>
                                            <div class="hole-avatar-box">
                                                        <%--头像--%>
                                                    <a href="#" class="avtm" target="_blank"><img
                                                            src="${ctx}/avatar/${comment.user.avatar}"/></a>

                                                </div>
                                            </div>
                                    <td class="plc">
                                        <div class="pi">

                                            <div id="fj" class="y">


                                            </div>
                                            <strong>
                                                <a href="#" id="postnum5897387"
                                                   onclick="setCopy(this.href, '帖子地址复制成功');return false;">
                                                    <em>${comment.cfloor}</em>楼</a>
                                            </strong>
                                            <div class="pti">
                                                <div class="pdbt">
                                                </div>
                                                <div class="authi">
                                                    <em id="authorposton5897387">发表时间　${comment.ctime}</em>
                                                    <span class="pipe"></span>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="pct">
                                            <style type="text/css">.pcb {
                                                margin-right: 0
                                            }</style>
                                            <div class="pcb">
                                                <div class="t_fsz">
                                                    <table cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="t_f js-thread-content" id="postmessage_5897387">
                                                                    ${comment.ccontent}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div id="comment_5897387" class="cm">
                                            </div>
                                            <div id="post_rate_div_5897387"></div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="plc plm">
                                        <div id="p_btn" class="mtw mbm hm cl">


                                        </div>
                                    </td>
                                </tr>
                                <tr id="_postposition5897387"></tr>
                                <tr>
                                    <td class="pls"></td>
                                    <td class="plc" style="overflow:visible;">
                                        <div class="po hin">
                                            <div class="pob cl">
                                                <em>
                                                    <a class="fastre floorre" href="javascript:;">回复</a>
                                                </em>

                                                <p>
                                                    <a href="javascript:;">举报</a>
                                                </p>

                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="ad">
                                    <td class="pls">
                                    </td>
                                    <td class="plc">
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div id="postlistreply" class="pl">
                            <div id="post_new" class="viewthread_table" style="display: none"></div>
                        </div>
                    </div>
                </c:otherwise>
            </c:choose>
        </c:forEach>


        <form method="post" autocomplete="off" name="modactions" id="modactions">
            <input type="hidden" name="formhash" value="6048d1bc"/>
            <input type="hidden" name="optgroup"/>
            <input type="hidden" name="operation"/>
            <input type="hidden" name="listextra" value="page%3D1"/>
            <input type="hidden" name="page" value="1"/>
        </form>
        <c:if test="${maxpage>page}">
            <div class="pgbtn">
                <a href="comment.jsp?pid=${post.pid}&page=${page+1}" hidefocus="true" class="bm_h">
                    下一页 »
                </a>
            </div>
        </c:if>


        <div class="pgs mtm mbm cl">
            <div class="pg">
                <c:if test="${page>10}">
                    <a href="${ctx}/comment.jsp?pid=${post.pid}&page=1">1..</a>
                </c:if>
                <c:if test="${page>1}">
                    <a href="${ctx}/comment.jsp?pid=${post.pid}&page=${page-1}" class="prev">&nbsp;&nbsp;</a>
                </c:if>
                <fmt:parseNumber var="pagediv" integerOnly="true" value="${page/10}" />
                <c:forEach var="i" begin="${pagediv*10+1}" end="${maxpage<=10?maxpage:(pagediv*10+10)}" step="1">
                    <c:choose>
                        <c:when test="${i==page}">
                            <strong>${i}</strong>
                        </c:when>
                        <c:when test="${i<=maxpage}">
                            <a href="${ctx}/comment.jsp?pid=${post.pid}&page=${i}">${i}</a>
                        </c:when>
                    </c:choose>
                </c:forEach>
                <c:if test="${maxpage>10&&maxpage!=page}">
                    <a href="${ctx}/comment.jsp?pid=${post.pid}&page=${maxpage}">..${maxpage}</a>
                </c:if>
                <label>
                    <input type="text" name="custompage" class="px" size="2" title="输入页码，按回车快速跳转" value="${page}" onkeydown="if(event.keyCode==13) {window.location='comment.jsp?pid=${post.pid}&page='+this.value;; doane(event);}">
                    <span title="共 ${maxpage} 页"> / ${maxpage} 页</span>
                </label>
                <c:if test="${maxpage>page}">
                <a href="${ctx}/comment.jsp?pid=${post.pid}&page=${page+1}" class="nxt">下一页</a>
                </c:if>
            </div>
            <span class="pgb y"><a href="${ctx}/post.jsp?sid=${post.sid}">返回列表</a></span>
            <a id="newspecialtmp" class="btn-default" href="${ctx}/post.jsp?sid=${post.sid}" title="发新帖">发帖</a>
            <a class="btn-default" id="post_reply" href="javascript:;" title="回复">回复</a>
        </div>

        <!--[diy=diyfastposttop]-->
        <div id="diyfastposttop" class="area"></div><!--[/diy]-->
        <script type="text/javascript">
            var postminchars = parseInt('2');
            var postmaxchars = parseInt('200000');
            var disablepostctrl = parseInt('0');
        </script>

        <div id="f_pst" class="pl bm bmw">
            <form method="post" autocomplete="off" id="fastpostform" action="#">
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="pls">
                            <div class="avatar avtm">
                                <c:choose>
                                    <c:when test="${empty sessionScope.user}">
                                        <img src="${ctx}/avatar/defaultavatar.png"/>
                                    </c:when>
                                    <c:otherwise>
                                        <img src="${ctx}/avatar/${sessionScope.user.avatar}"/>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </td>
                        <td class="plc">

                            <span id="fastpostreturn"></span>


                            <div class="cl">
                                <div id="fastsmiliesdiv" class="y">
                                    <div id="fastsmiliesdiv_data">
                                        <div id="fastsmilies"></div>
                                    </div>
                                </div>
                                <div class="hasfsl" id="fastposteditor">
                                    <div class="tedt mtn" style="width:720px;height:236px;overflow: hidden;">
                                        <%=htmlData%>
                                        <textarea name="content" class="form-control" cols="100" rows="8"
                                                  style="width:720px;height:245px;visibility:hidden;"><%=htmlspecialchars(htmlData)%></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="mtm sec">
                            </div>
                            <p class="ptm pnpost">
                                <a href="#" class="y" target="_blank">本版积分规则</a>
                                <button type="button" name="replysubmit" id="fastpostsubmit" class="pn pnc vm"
                                        value="replysubmit" tabindex="5"><strong>发表回复</strong></button>
                                <label for="fastpostrefresh"><input id="fastpostrefresh" type="checkbox" class="pc"/>回帖后跳转到最后一页</label>
                            </p>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>

</div>


<div class="footer">
    <div class="container-sm">
    </div>
</div>

</body>
</html>
<%!
    private String htmlspecialchars(String str) {
        str = str.replaceAll("&", "&amp;");
        str = str.replaceAll("<", "&lt;");
        str = str.replaceAll(">", "&gt;");
        str = str.replaceAll("\"", "&quot;");
        return str;
    }
%>
