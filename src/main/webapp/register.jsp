<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>论坛注册界面</title>
    <link rel="stylesheet" type="text/css" href="${ctx}/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="${ctx}/css/login.css">
    <script src="${ctx}/js/jquery-3.3.1.js"></script>
    <script src="${ctx}/js/bootstrap.min.js"></script>
    <script src="${ctx}/js/register.js"></script>
</head>
<body>
<div class="wrap">
    <div class="container">
        <h1>Welcome</h1>
        <form action="${ctx}/user?userop=register" method="post">
            <div class="input"><input type="text" class="form-control" placeholder="输入用户名" name="username">
                <div class="usercheck"></div>
                <div class="checknull"></div>
            </div>
            <div class="input"><input type="text" class="form-control" placeholder="输入昵称" name="nickname">
                <div class="nickcheck"></div>
            </div>
            <div class="input"><input type="password" class="form-control" placeholder="输入密码" name="password">
                <div class="pwdcheck1"></div>
            </div>
            <div class="input"><input type="password" class="form-control" placeholder="再次输入密码" name="password2">
                <div class="pwdcheck2"></div>
            </div>
            <div class="input"><input type="submit" class="btn btn-default" id="register" value="注册"/></div>
            <div class="input"><input id="login" class="btn btn-default" type="button" value="已有帐号？登录"/></div>
        </form>
    </div>
    <ul>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
    </ul>
</div>
</body>
</html>