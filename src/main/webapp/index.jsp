<%--
  Created by IntelliJ IDEA.
  User: YE
  Date: 2018/5/25
  Time: 17:23
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>论坛首页   </title>

    <link rel="stylesheet" type="text/css" href="${ctx}/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/css/utils.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/css/mobile.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/css/jquery-ui.css"/>
    <script src="${ctx}/js/jquery-3.3.1.js"></script>
    <script src="${ctx}/js/require.js"></script>
    <script src="${ctx}/js/utils.js"></script>
    <script src="${ctx}/js/app.js"></script>


</head>

<body>
<c:if test="${empty requestScope.posts}">
    <jsp:forward page="post?postop=index"></jsp:forward>
</c:if>
<c:set value="${requestScope.posts}" var="posts"></c:set>
<c:set value="${requestScope.sections}" var="sections"></c:set>
<div class="pop-dialog" id="popQR">
    <div id="popMask" class="dialog-mask" style="display: block;"></div>
    <div class="dialog-box" style="display: block; margin-top: -210px;">
        <div class="dialog-title">
            <h4 class="js-title">可以直接扫描二维码安装</h4>
            <a id="btnClose" class="dialog-close"></a>
        </div>
        <div class="dialog-content" style="height: auto;">
            <div id="dialogQR"></div>

        </div>
    </div>
</div>

<!-- 兼容旧版 start -->
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<!-- 兼容旧版 end -->

<div id="toptb" class="cl" style="display: none;">
    <div class="wp">
        <div class="z"><script type="text/javascript">var _speedMark = new Date();</script></div>
        <div class="y">
            <a id="switchblind" href="javascript:;" onclick="toggleBlind(this)" title="开启辅助访问" class="switchblind">开启辅助访问</a>
        </div>
    </div>
</div>

<div id="qmenu_menu" class="p_pop blk" style="display: none;">
    <div class="ptm pbw hm">
        <a href="${ctx}/login.jsp" class="xi2" ><strong>登录</strong></a>
        <a href="${ctx}/register.jsp" class="xi2 xw1">立即注册</a>
    </div>
</div><div class="header-sm">
    <div class="header-main">
        <div class="container-sm clearfix">


            <!-- use login status -->
            <div class="user">
                <c:choose>
                    <c:when test="${empty sessionScope.user}">
                        <form autocomplete="off" id="lsform" class="ui-member">
                            <div class="fastlg cl">
                                <div>
                                    <span class="xi2">
                                        <a id="user-login" href="${ctx}/login.jsp" class="xi2">登录</a>
                                    </span>
                                    <span class="pipe">|</span>
                                    <span class="xi2">
                                        <a id="user-register" href="${ctx}/register.jsp">注册</a>
                                    </span>
                                </div>

                            </div>
                        </form>
                    </c:when>
                    <c:otherwise>
                        <div id="um">
                            <div class="ui-login-toggle">
                                <span class="user-avatar"><img src="${ctx}/avatar/${sessionScope.user.avatar}"/></span>
                                <span class="user-name hide-row">${sessionScope.user.nickname}</span>
                            </div>
                            <div class="ui-login-status">
                                <ul>
                                    <li class="user-primary-info">
                                        <p class="user-avatar-name">
                                    <span class="user-avatar">
                                        <a href="javascript:;"><img src="${ctx}/avatar/${sessionScope.user.avatar}"/></a>
                                    </span> <span class="hide-row">
                                    <a href="javascript:;" target="_self"
                                       class="hide-row">${sessionScope.user.nickname}</a></span>
                                        </p>
                                        <p class="user-pipe"></p>
                                    </li>
                                    <c:if test="${sessionScope.user.permission==99}">
                                        <li class="user-setting"><a href="${ctx}/admin.jsp">后台管理</a></li>
                                    </c:if>
                                    <li class="user-message"><a href="${ctx}/home.jsp?uid=${sessionScope.user.uid}">个人主页</a>
                                    </li>
                                    <li class="user-setting"><a href="${ctx}/setting.jsp">个人设置</a></li>
                                    <li class="user-logout"><a href="${ctx}/user?userop=logout">退出账号</a></li>
                                </ul>
                            </div>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>

            <ul class="header-menu">
                <li><a target="_self" href="${ctx}/index.jsp">论坛首页</a></li>
                <li><a target="_self" href="${ctx}/forum.jsp">论坛版块</a></li>
            </ul>

        </div>

        <!-- important { -->
        <div class="p_pop h_pop" id="mn_userapp_menu" style="display: none"></div><!-- } -->


    </div>
    <div id="forumList" class="header-sub">
        <div class="container-sm">


        </div>
    </div>



    <div class="location">
        <div class="container-sm clearfix"><div id="scbar" class="cl">
            <form id="scbar_form" method="post" autocomplete="off"  target="_blank">

            </form>
        </div>
            <ul id="scbar_type_menu" class="p_pop" style="display: none;"><li><a href="javascript:;" rel="user">用户</a></li></ul>
            <h2>
                <a href="${ctx}/index.jsp">论坛首页</a><div class="z"></div>
            </h2>
        </div>
    </div>

</div>
<div id="main" class="main clearfix" data-page="discuz">
    <div class="container-sm container-banner">
        <div id="indexBanner" class="banner clearfix">
            <style type="text/css">
                #banner{}
                .pic{width: 100%;}
                .pic ul li{display: none;}
                .pic .imgon{display: block;}
                .pic ul li img{width: 100%;position: relative;}
                .tab{position: absolute;bottom: 20px;left: 42%;}
                .tab .tabon{background: #f60;}
                .tab ul li{display: inline-block;width: 15px;height: 15px;border-radius: 50%;background: rgba(0,0,0,.5);cursor: pointer;margin-right: 10px;}
            </style>
            <div id="banner" onselectstart = 'return false'>
                <div class="pic">
                    <ul>
                        <li class="imgon"><img src="${ctx}/img/01.jpg" alt="" ></li>
                        <li><img src="${ctx}/img/02.jpg" alt="" /></li>
                        <li><img src="${ctx}/img/03.jpg" alt="" /></li>
                        <li><img src="${ctx}/img/04.jpg" alt="" /></li>
                        <li><img src="${ctx}/img/05.jpg" alt="" /></li>
                    </ul>
                </div>
                <div class="tab">
                    <ul>
                        <li class="tabon"></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                </div>
            </div>

            <script type="text/javascript">
                ~function(){
                    var $tabli = $("#banner .tab ul li");
                    var $picli = $("#banner .pic ul li");
                    var index =0;
                    $tabli.click(function(){
                        index = $(this).index();
                        $tabli.eq(index).addClass('tabon').siblings().removeClass('tabon');

                        $picli.eq(index).addClass('imgon').siblings().removeClass('imgon');
                    })
                } ();
            </script>
            <div class="banner-border js-banner-border"></div>
        </div>
    </div>

    <div class="container-sm clearfix">
        <div id="newsBox" class="section">
            <div id="discussBox" class="foldableBox discuss " data-node="fold_discuss">
                <h4>热帖推荐<span class="btn-fold"></span></h4>
                <div class="js-list">
                    <c:forEach var="post" items="${posts}">
                        <div class="discuss-box" style="border-top: none;">
                            <div class="portrait">
                                <a href="${ctx}/home.jsp?uid=${post.user.uid}">
                                    <img src="${ctx}/avatar/${post.user.avatar}">
                                </a>
                            </div>

                            <div class="discuss-content">
                                <h5>
                                    <a href="${ctx}/comment.jsp?pid=${post.pid}" target="_blank">${post.ptitle}</a>
                                </h5>
                                <ul class="post-info">
                                    <li class="author"><a href="${ctx}/home.jsp?uid=${post.user.uid}">${post.user.nickname}</a></li>
                                    <li class="time">发表于 ${post.pcreatetime}</li>
                                </ul>
                            </div>
                        </div>
                    </c:forEach>
                </div>

                <div class="forum-list-pager js-pager" data-total="56"></div>

            </div>
        </div>
        <div class="sidebar">

            <div class="right-module plate-recommend">
                <h4>版块推荐</h4>
                <ul class="plate-list clearfix">
                    <c:forEach var="section" items="${sections}">
                        <li style="background: url(img/ori.png) no-repeat 20px center;background-size: 28px 28px;-webkit-background-size: 28px 28px;">
                            <a href="${ctx}/post.jsp?sid=${section.sid}" target="_blank">${section.sname}</a>
                        </li>
                    </c:forEach>
                </ul>
            </div>

            <div class="right-module offical-SNS">
                <h4>关注我们</h4>
                <ul class="offical-SNS-list clearfix">
                    <li class="offical-SNS-weibo">
                        <a href="#" target="_blank">
                            论坛官方微博
                        </a>
                    </li>
                    <li class="offical-SNS-wechat">
                        <a class="qr-trigger" data-href="#"
                           data-title="打开手机，使用微信扫描二维码"
                           data-summary="关注官方服务号，即可通过手机微信完成很多操作。">
                            官方微信
                        </a>
                    </li>
                </ul>
                <ul class="offical-app-list">
                    <li style="border-bottom: none; margin-top: -10px;">
                        <img src="${ctx}/img/图标.jpg" alt="" data-evernote-hover-show="true">
                        <dl>
                            <dt><a target="_blank" href="#">官方论坛</a></dt>
                            <dd>不是普通论坛。</dd>
                        </dl>
                        <a class="qr-trigger" data-href="#"></a>
                    </li>
                </ul>
            </div>

        </div>
    </div>
</div>


<!-- *************** 页面底部 start **************** --><div class="footer">
    <div class="container-sm">
        <div class="link clearfix">
            <ul class="clearfix">
                <li><a href="http://www.baidu.com" target="_blank" title="百度">百度</a></li>
                <li><a href="http://http://bbs.tianya.cn/" target="_blank" title="天涯社区">天涯社区</a></li>
                <li><a href="http://www.zjweu.edu.cn/" target="_blank" title="浙江水利水电学院">浙江水利水电学院</a></li>
                <li><a href="http://bbs.gfan.com" target="_blank" title="机锋安卓论坛">机锋安卓论坛</a></li>
                <li><a href="http://www.ali213.net/" target="_blank" title="游侠网">游侠网</a></li>
                <li><a href="http://game.qq.com/" target="_blank" title="腾讯游戏">腾讯游戏</a></li>
                <li><a href="http://www.lovebizhi.com/android.html" target="_blank" title="爱壁纸">爱壁纸</a></li>
                <li><a href="https://movie.douban.com/" target="_blank" title="豆瓣网">豆瓣网</a></li>
                <li><a href="http://jiyouhui.it168.com" target="_blank" title="机友会">机友会</a></li>
                <li><a href="http://bbs.togic.com/forum.php" target="_blank" title="泰捷论坛">泰捷论坛</a></li>
                <li><a href="https://www.zhihu.com/" target="_blank" title="知乎">知乎</a></li>
                <li><a href="http://android.ithome.com" target="_blank" title="安卓之家">安卓之家</a></li>
                <li><a href="http://bbs.maxthon.cn" target="_blank" title="傲游社区">傲游社区</a></li>
                <li><a href="http://bbs.dospy.com " target="_blank" title="DOSPY论坛">DOSPY论坛</a></li>
                <li><a href="http://bbs.m.qq.com" target="_blank" title="手机管家论坛">手机管家论坛</a></li>
                <li><a href="http://sports.sina.com.cn/" target="_blank" title="新浪体育">新浪体育</a></li>
                <li><a href="http://www.zhanqi.tv/" target="_blank" title="战旗tv">战旗tv</a></li>
                <li><a href="http://www.7to.cn" target="_blank" title="奇兔刷机">奇兔刷机</a></li>
                <li><a href="http://bbs.ydss.cn" target="_blank" title="移动叔叔论坛">移动叔叔论坛</a></li>
                <li><a href="http://bbs.duba.net" target="_blank" title="爱毒霸论坛">爱毒霸论坛</a></li>
                <li><a href="https://y.qq.com/" target="_blank" title="QQ音乐">QQ音乐</a></li>
                <li><a href="http://bbs.yj.youku.com" target="_blank" title="优酷智能硬件">优酷智能硬件</a></li>
                <li><a href="http://bbs.smartisan.com/thread-106095-1-1.html" target="_blank" title="申请友情链接">申请友情链接</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="back-top"></div>


</body>
</html>

