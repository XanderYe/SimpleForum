<%--
  Created by IntelliJ IDEA.
  User: YE
  Date: 2018/5/23
  Time: 10:22
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>个人资料</title>
</head>
<link rel="stylesheet" type="text/css" href="${ctx}/css/common.css"/>
<link rel="stylesheet" type="text/css" href="${ctx}/css/home.css"/>
<link rel="stylesheet" type="text/css" href="${ctx}/css/utils.css"/>
<link rel="stylesheet" type="text/css" href="${ctx}/css/main.css"/>
<link rel="stylesheet" type="text/css" href="${ctx}/css/mobile.css"/>
<link rel="stylesheet" type="text/css" href="${ctx}/css/jquery-ui.css"/>
<script src="${ctx}/js/jquery-3.3.1.js"></script>
<script src="${ctx}/js/jquery-ui.js"></script>
<script src="${ctx}/js/setting.js"></script>

</head>

<body id="nv_home" class="pg_spacecp" onkeydown="if(event.keyCode==27) return false;">
<c:if test="${empty sessionScope.user}">
    <jsp:forward page="login.jsp"></jsp:forward>
</c:if>
<!-- 兼容旧版 start -->
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<!-- 兼容旧版 end -->

<div id="toptb" class="cl" style="display: none;">
    <div class="wp">
        <div class="z"></div>
        <div class="y">
            <a id="switchblind" href="javascript:;" onclick="toggleBlind(this)" title="开启辅助访问" class="switchblind">开启辅助访问</a>
        </div>
    </div>
</div>

<ul id="myprompt_menu" class="p_pop" style="display: none;">
    <li><a href="#" id="pm_ntc" style="background-repeat: no-repeat; background-position: 0 50%;"><em
            class="prompt_news_0"></em>消息</a></li>

    <li><a href="#"><em class="prompt_follower_0"></em>新听众</a></li>

    <li class="ignore_noticeli"><a href="javascript:;" onclick="setcookie('ignore_notice', 1);hideMenu('myprompt_menu')"
                                   title="暂不提醒"><em class="ignore_notice"></em></a></li>
</ul>
<div id="qmenu_menu" class="p_pop " style="display: none;">
    <ul class="cl nav"></ul>
</div>
<div class="header-sm">
    <div class="header-main">
        <div class="container-sm clearfix">


            <!-- use login status -->
            <div class="user">
                <div id="um">

                    <!-- 头像 -->
                    <div class="ui-login-toggle">
                        <span class="user-avatar"><img src="${ctx}/avatar/${sessionScope.user.avatar}"/></span>
                        <span class="user-name hide-row">${sessionScope.user.nickname}</span>
                    </div>


                    <div class="ui-login-status">
                        <ul>
                            <li class="user-primary-info">
                                <p class="user-avatar-name">
                                    <span class="user-avatar"><a href="javascript:;"><img
                                            src="${ctx}/avatar/${sessionScope.user.avatar}"/></a></span>
                                    <span class="hide-row"><a href="javascript:;" target="_self"
                                                              class="hide-row">${sessionScope.user.nickname}</a></span>
                                </p>
                                <p class="user-pipe">
                                </p>
                            </li>
                            <c:if test="${sessionScope.user.permission==99}">
                                <li class="user-setting"><a href="${ctx}/admin.jsp">后台管理</a></li>
                            </c:if>
                            <li class="user-message"><a href="${ctx}/home.jsp?uid=${sessionScope.user.uid}">个人主页</a></li>
                            <li class="user-setting"><a href="${ctx}/setting.jsp">个人设置</a></li>
                            <li class="user-logout"><a href="${ctx}/user?userop=logout">退出账号</a></li>
                        </ul>

                    </div>
                </div>
            </div>

            <ul class="header-menu">


                <li><a href="${ctx}/index.jsp">论坛首页</a></li>
                <li id="header-fold"><a href="${ctx}/forum.jsp">论坛版块</a></li>
            </ul>


        </div>

        <div class="p_pop h_pop" id="mn_userapp_menu" style="display: none"></div><!-- } -->


    </div>
    <div id="forumList" class="header-sub">
        <div class="container-sm">


        </div>
    </div>
    <div class="location">
        <div class="container-sm clearfix">
            <div id="scbar" class="cl">
                <form id="scbar_form" method="post" autocomplete="off" onsubmit="searchFocus($('scbar_txt'))"
                      action="search.php?searchsubmit=yes" target="_self">
                    <input type="hidden" name="mod" id="scbar_mod" value="search"/>
                    <input type="hidden" name="formhash" value="6048d1bc"/>
                    <input type="hidden" name="srchtype" value="title"/>
                    <input type="hidden" name="srhfid" value="0"/>
                    <input type="hidden" name="srhlocality" value="home::spacecp"/>
                    <table cellspacing="0" cellpadding="0">
                    </table>
                </form>
            </div>
            <ul id="scbar_type_menu" class="p_pop" style="display: none;">
                <li><a href="javascript:;" rel="forum" class="curtype">帖子</a></li>
                <li><a href="javascript:;" rel="user">用户</a></li>
            </ul>
            <h2>
                <a href="${ctx}/index.jsp">论坛首页</a>
            </h2>
        </div>
    </div>
</div>


<div id="wp" class="wp">
    <div id="pt" class="bm cl">
        <div class="z">
            <a href="${ctx}/index.jsp" class="nvhm" title="首页">论坛</a> <em>&rsaquo;</em>
            <a href="#">设置</a> <em>&rsaquo;</em>个人资料
        </div>
    </div>
    <div id="ct" class="ct2_a wp cl">
        <div class="mn">
            <div class="bm bw0">
                <h1 class="mt">个人资料
                </h1>
                <!--don't close the div here-->
                <ul class="tb cl">
                    <li class="a"><a href="#">完善个人信息</a></li>
                </ul>
                <iframe id="frame_profile" name="frame_profile" style="display: none"></iframe>
                <form action="${ctx}/user?userop=changepwd" method="post">
                    <table cellspacing="0" cellpadding="0" class="tfm" id="profilelist">
                        <tr>
                            <th>旧密码</th>
                            <td>
                                <input type="password" size="40px" name="oldpwd">
                                <div class="rq mtn" id="showerror_realname"></div>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <th>新密码</th>
                            <td>
                                <input type="password" size="40px" name="password">
                                <div class="rq mtn" id="showerror_realname"></div>
                            </td>
                            <td class="p">
                            </td>
                        </tr>
                        <tr>
                            <th>重复密码</th>
                            <td>
                                <input type="password" size="40px" name="password2">
                                <div class="rq mtn" id="showerror_realname"></div>
                            </td>
                            <td class="p">
                            </td>
                        </tr>
                        <tr>
                            <th>&nbsp;</th>
                            <td colspan="2">
                                <input type="hidden" name="profilesubmit" value="true"/>
                                <button type="submit" name="profilesubmitbtn" id="profilesubmitbtn" value="true"
                                        class="pn pnc"/>
                                <strong>修改</strong></button>
                                <span id="submit_result" class="rq"></span>
                            </td>
                        </tr>
                    </table>
                </form>
                <script>
                    $(function () {
                        $("#profilesubmitbtn").click(function () {
                            var old=$("input[name=oldpwd]").val();
                            var password=$("input[name=password]").val();
                            var password2=$("input[name=password2]").val();
                            if(password!=password2){
                                alert("两次密码不一致！");
                                return false;
                            }
                            if(old==password){
                                alert("新旧密码不能相同！");
                                return false;
                            }
                            return true;
                        });
                    });
                </script>

            </div>
        </div>
        <div class="appl">
            <div class="tbn">
                <h2 class="mt bbda">设置</h2>
                <ul>
                    <li><a href="${ctx}/avatar.jsp">修改头像</a></li>
                    <li><a href="${ctx}/setting.jsp">个人资料</a></li>
                    <li class="a"><a href="${ctx}/changepwd.jsp">修改密码</a></li>


                </ul>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">checkBlind();</script>
</body>
</html>
