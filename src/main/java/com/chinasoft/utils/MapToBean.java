package com.chinasoft.utils;

import com.chinasoft.dao.Impl.TopicDAOImpl;
import com.chinasoft.dao.Impl.UserDAOImpl;
import com.chinasoft.dao.TopicDAO;
import com.chinasoft.dao.UserDAO;
import com.chinasoft.entity.Comment;
import com.chinasoft.entity.Post;
import com.chinasoft.entity.Topic;
import com.chinasoft.entity.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by YE on 2018/5/17.
 */
public class MapToBean {
    static TopicDAO topicDAO=new TopicDAOImpl();
    static UserDAO userDAO=new UserDAOImpl();
    public static Comment changeToComment(Map map){
        Comment comment=new Comment();
        comment.setCid((Integer) map.get("cid"));
        comment.setPid((Integer) map.get("pid"));
        comment.setCfloor((Integer) map.get("cfloor"));
        comment.setUser((User) map.get("user"));
        comment.setCcontent((String) map.get("ccontent"));
        comment.setCtime((String) map.get("ctime"));
        return comment;
    }
    public static Post changeToPost(Map map){
        Post post=new Post();
        post.setPid((Integer) map.get("pid"));
        post.setSid((Integer) map.get("sid"));
        post.setTopic((Topic) map.get("topic"));
        post.setPtitle((String) map.get("ptitle"));
        post.setUser((User) map.get("user"));
        post.setPcreatetime((String) map.get("pcreatetime"));
        post.setIsstick((int) map.get("isstick"));
        return post;
    }
    public static List<Post> MapOp(List<Map> li) {
        List<Post> list=new ArrayList<>();
        for (Map map : li) {
            int tid = (int) map.get("tid");
            map.remove(tid);
            Topic topic = topicDAO.selectTopic(tid);
            map.put("topic", topic);
            int uid = (int) map.get("uid");
            map.remove(uid);
            User user = userDAO.selectUser(uid);
            map.put("user", user);
            Post post = MapToBean.changeToPost(map);
            list.add(post);
        }
        return list;
    }
}
