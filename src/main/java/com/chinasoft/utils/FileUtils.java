package com.chinasoft.utils;

import java.io.File;
import java.io.IOException;

/**
 * Created by YE on 2018/5/27.
 */
public class FileUtils {

    public static boolean find(String pathName,String filename) throws IOException{
        int filecount=0;
        //获取pathName的File对象
        File dirFile = new File(pathName);
        //判断该文件或目录是否存在，不存在时在控制台输出提醒
        if (!dirFile.exists()) {
            return false;
        }
        //获取此目录下的所有文件名与目录名
        String[] fileList = dirFile.list();
        for (int i = 0; i < fileList.length; i++) {
            //遍历文件目录
            String string = fileList[i];
            //File("documentName","fileName")是File的另一个构造器
            File file = new File(dirFile.getPath(),string);
            String name = file.getName();
            if(name.equals(filename)){
                return true;
            }
        }
        return false;
    }

}
