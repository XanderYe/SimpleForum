package com.chinasoft.dao;

import com.chinasoft.entity.Topic;

import java.util.List;

/**
 * Created by YE on 2018/5/18.
 */
public interface TopicDAO {
    public Topic selectTopic(int tid);

    public int addTopic(Topic topic);

    public int updateTopic(Topic topic);

    public int deleteTopic(int tid);

    public List<Topic> getTopics();

    public List<Topic> getTopicsBySid(int sid);
}
