package com.chinasoft.dao;

import com.chinasoft.entity.Post;

import java.util.List;

/**
 * Created by YE on 2018/5/18.
 */
public interface PostDAO {
    public int addPost(Post post);

    public int deletePost(int pid);

    public List<Post> getAllPosts(int start,int length);

    public List<Post> getPostsBySid(int sid,int start,int length);

    public int getPostsCountBySid(int sid,int length) ;

    public List<Post> getPostsByTid(int tid,int start,int length);

    public int getPostsCountByTid(int tid,int length);

    public Post getPostsByPid(int pid);

    public Post selectPost(int pid);

}
