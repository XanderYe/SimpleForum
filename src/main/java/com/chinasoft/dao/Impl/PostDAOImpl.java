package com.chinasoft.dao.Impl;

import com.chinasoft.dao.PostDAO;
import com.chinasoft.dao.TopicDAO;
import com.chinasoft.dao.UserDAO;
import com.chinasoft.entity.Post;
import com.chinasoft.entity.Topic;
import com.chinasoft.entity.User;
import com.chinasoft.utils.DbUtils;
import com.chinasoft.utils.MapToBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by YE on 2018/5/18.
 */
public class PostDAOImpl implements PostDAO {
    TopicDAO topicDAO=new TopicDAOImpl();
    UserDAO userDAO = new UserDAOImpl();
    List<Post> list = null;

    @Override
    public int addPost(Post post) {
        String sql = "insert into post (sid,tid,ptitle,uid,pcreatetime)values(?,?,?,?,?) ";
        return DbUtils.getId(sql, post.getSid(), post.getTopic().getTid(), post.getPtitle(), post.getUser().getUid(), post.getPcreatetime());
    }

    @Override
    public int deletePost(int pid) {
        String sql = "delete from post where pid=?";

        return DbUtils.update(sql, pid);
    }

    @Override
    public List<Post> getAllPosts(int start,int length) {
        String sql = "select *from post ORDER BY pid DESC LIMIT ?,?";
        list = new ArrayList<>();
        List<Map> li = DbUtils.getMaps(sql,start,length);
        return MapToBean.MapOp(li);
    }

    @Override
    public List<Post> getPostsBySid(int sid,int start,int length) {
        list = new ArrayList<>();
        String sql = "select * from post where sid=? ORDER BY pid DESC LIMIT ?,?";
        List<Map> li = DbUtils.getMaps(sql, sid,start,length);
        return MapToBean.MapOp(li);
    }

    @Override
    public int getPostsCountBySid(int sid,int length) {
        list = new ArrayList<>();
        String sql = "select count(*) from post where sid=? ORDER BY pid";
        int len=DbUtils.getCount(sql, sid);
        if(len%length>0){
            return len/length+1;
        }
        return len/length;
    }

    @Override
    public List<Post> getPostsByTid(int tid,int start,int length) {
        list = new ArrayList<>();
        String sql = "select * from post where tid=? ORDER BY pid DESC LIMIT ?,?";
        List<Map> li = DbUtils.getMaps(sql, tid,start,length);
        return MapToBean.MapOp(li);
    }

    @Override
    public int getPostsCountByTid(int tid,int length) {
        String sql = "select count(*) from post where tid=? ORDER BY pid";
        int len=DbUtils.getCount(sql, tid);
        if(len%length>0){
            return len/length+1;
        }
        return len/length;
    }

    @Override
    public Post getPostsByPid(int pid) {
        list = new ArrayList<>();
        String sql = "select * from post where pid=?";
        Map map = DbUtils.getMap(sql, pid);
        int tid = (int) map.get("tid");
        map.remove(tid);
        Topic topic = topicDAO.selectTopic(tid);
        map.put("topic", topic);
        int uid = (int) map.get("uid");
        map.remove(uid);
        User user = userDAO.selectUser(uid);
        map.put("user", user);
        return MapToBean.changeToPost(map);
    }

    @Override
    public Post selectPost(int pid) {
        String sql="select * from post where pid=?";
        return (Post) DbUtils.getObject(sql,Post.class,pid);
    }
}
