package com.chinasoft.dao.Impl;

import com.chinasoft.dao.TopicDAO;
import com.chinasoft.entity.Topic;
import com.chinasoft.utils.DbUtils;

import java.util.List;

/**
 * Created by YE on 2018/5/18.
 */
public class TopicDAOImpl implements TopicDAO {
    @Override
    public Topic selectTopic(int tid) {
        String sql = "select * from topic where tid=?";
        return (Topic) DbUtils.getObject(sql, Topic.class, tid);
    }

    @Override
    public int addTopic(Topic topic) {
        String sql = "insert into topic (sid,tname)values(?,?)";
        return DbUtils.update(sql, topic.getSid(), topic.getTname());
    }

    @Override
    public int updateTopic(Topic topic) {
        String sql = "update topic set sid =?,tname=? where tid=?";
        return DbUtils.update(sql, topic.getSid(), topic.getTname(), topic.getTid());
    }

    @Override
    public int deleteTopic(int tid) {
        String sql = "delete from topic where tid=?";
        return DbUtils.update(sql, tid);
    }

    @Override
    public List<Topic> getTopics() {
        String sql = "select * from topic";
        return DbUtils.getObjects(sql, Topic.class);
    }

    @Override
    public List<Topic> getTopicsBySid(int sid) {
        String sql = "select * from topic where sid=?";
        return DbUtils.getObjects(sql, Topic.class, sid);
    }
}
