package com.chinasoft.dao.Impl;

import com.chinasoft.dao.UserDAO;
import com.chinasoft.entity.User;
import com.chinasoft.utils.DbUtils;

import java.util.List;

/**
 * Created by YE on 2018/5/17.
 */
public class UserDAOImpl implements UserDAO {
    @Override
    public User selectUser(int uid) {
        String sql = "select * from user where uid=?";
        return (User) DbUtils.getObject(sql, User.class, uid);
    }

    @Override
    public User selectUser(String username) {
        String sql = "select * from user where username=?";
        return (User) DbUtils.getObject(sql, User.class, username);
    }

    @Override
    public boolean isExist(String username) {
        String sql = "select * from user where username=?";
        User user = (User) DbUtils.getObject(sql, User.class, username);
        if (user != null) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isValid(User user) {
        String sql = "select * from user where username=? and password=?";
        User userlist = (User) DbUtils.getObject(sql, User.class, user.getUsername(), user.getPassword());
        if (userlist != null) {
            return true;
        }
        return false;
    }

    @Override
    public int addUser(User user) {
        String sql = "insert  user (username,nickname,PASSWORD) VALUES (?,?,?)";
        return DbUtils.update(sql, user.getUsername(), user.getNickname(), user.getPassword());
    }

    @Override
    public int deleteUser(int uid) {
        String sql = "delete from user where uid=?";
        return DbUtils.update(sql, uid);

    }

    @Override
    public int updateUser(User user) {
        String sql="UPDATE user SET nickname=?,gender=?,birthday=?,address=? WHERE username=?";
        return DbUtils.update(sql,user.getNickname(),user.getGender(), user.getBirthday(), user.getAddress(),user.getUsername());
    }

    @Override
    public int updateUserByAdmin(User user) {
        String sql = "update  user set username=?,nickname=?,password=?,email=? where uid=?";
        return DbUtils.update(sql, user.getUsername(), user.getNickname(), user.getPassword(),user.getEmail(),user.getUid());

    }

    @Override
    public int updatePwd(User user) {
        String sql="UPDATE user SET password=? WHERE uid=?";
        return DbUtils.update(sql,user.getPassword(),user.getUid());
    }

    @Override
    public List<User> getAllUsers() {
        String sql = "select * from user";
        return DbUtils.getObjects(sql, User.class);
    }

    @Override
    public int updateEmail(User user) {
        String sql="UPDATE user SET email=? WHERE uid=?";
        return DbUtils.update(sql,user.getEmail(),user.getUid());
    }

    @Override
    public int verifyEmail(User user) {
        String sql="UPDATE user SET emailcheck=? WHERE uid=?";
        return DbUtils.update(sql,user.getEmailcheck(),user.getUid());
    }

    @Override
    public int updateAvatar(User user) {
        String sql="UPDATE user SET avatar=? WHERE uid=?";
        return DbUtils.update(sql,user.getAvatar(),user.getUid());
    }
}
