package com.chinasoft.dao.Impl;

import com.chinasoft.dao.SectionDAO;
import com.chinasoft.entity.Section;
import com.chinasoft.utils.DbUtils;

import java.util.List;

/**
 * Created by YE on 2018/5/21.
 */
public class SectionDAOImpl implements SectionDAO {

    @Override
    public Section selectSection(int sid) {
        String sql = "select * from section where sid=?";
        return (Section) DbUtils.getObject(sql, Section.class, sid);
    }

    @Override
    public int addSection(Section section) {
        String sql = "insert into section (sname) values (?)";
        return DbUtils.update(sql, section.getSname());
    }

    @Override
    public int updateSection(Section section) {
        String sql = "update section set sname=? where sid=?";
        return DbUtils.update(sql, section.getSname(), section.getSid());
    }

    @Override
    public int deleteSection(int sid) {
        String sql = "delete from section where sid=?";
        return DbUtils.update(sql, sid);
    }

    @Override
    public List<Section> getAllSections() {
        String sql = "select * from section";
        return DbUtils.getObjects(sql, Section.class);
    }

}
