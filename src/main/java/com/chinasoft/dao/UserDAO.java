package com.chinasoft.dao;

import com.chinasoft.entity.User;

import java.util.List;

/**
 * Created by YE on 2018/5/17.
 */
public interface UserDAO {
    public User selectUser(int uid);

    public User selectUser(String username);

    public boolean isExist(String username);

    public boolean isValid(User user);

    public int addUser(User user);

    public int deleteUser(int uid);

    public int updateUser(User user);

    public int updatePwd(User user);

    public int updateUserByAdmin(User user);

    public List<User> getAllUsers();

    public int updateEmail(User user);

    public int verifyEmail(User user);

    public int updateAvatar(User user);
}
