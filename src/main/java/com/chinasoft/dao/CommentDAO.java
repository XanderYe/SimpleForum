package com.chinasoft.dao;

import com.chinasoft.entity.Comment;

import java.util.List;

/**
 * Created by YE on 2018/5/17.
 */
public interface CommentDAO {
    public int addComment(Comment comment);

    public int deleteComment(int cid);

    public Comment getComment(int cid);

    public int updateComment(Comment comment);

    public int getCount(int pid);

    public List<Comment> getAllComments(int pid,int start,int length);

    public int getCommentCount(int pid,int length);

}
