package com.chinasoft.dao;

import com.chinasoft.entity.Section;

import java.util.List;

/**
 * Created by YE on 2018/5/21.
 */
public interface SectionDAO {
    public Section selectSection(int sid);

    public int addSection(Section section);

    public int updateSection(Section section);

    public int deleteSection(int sid);

    public List<Section> getAllSections();
}
