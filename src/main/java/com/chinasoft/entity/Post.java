package com.chinasoft.entity;

/**
 * Created by YE on 2018/5/18.
 */
public class Post {
    private int pid;
    private int sid;
    private Topic topic;
    private String ptitle;
    private User user;
    private String pcreatetime;
    private int isstick;

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public String getPtitle() {
        return ptitle;
    }

    public void setPtitle(String ptitle) {
        this.ptitle = ptitle;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getPcreatetime() {
        return pcreatetime;
    }

    public void setPcreatetime(String pcreatetime) {
        this.pcreatetime = pcreatetime;
    }

    public int getIsstick() {
        return isstick;
    }

    public void setIsstick(int isstick) {
        this.isstick = isstick;
    }
}
