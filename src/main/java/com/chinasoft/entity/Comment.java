package com.chinasoft.entity;

/**
 * Created by YE on 2018/5/17.
 */
public class Comment {
    private int cid;
    private int pid;
    private int cfloor;
    private User user;
    private String ccontent;
    private String ctime;

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public int getCfloor() {
        return cfloor;
    }

    public void setCfloor(int cfloor) {
        this.cfloor = cfloor;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getCcontent() {
        return ccontent;
    }

    public void setCcontent(String ccontent) {
        this.ccontent = ccontent;
    }

    public String getCtime() {
        return ctime;
    }

    public void setCtime(String ctime) {
        this.ctime = ctime;
    }
}
