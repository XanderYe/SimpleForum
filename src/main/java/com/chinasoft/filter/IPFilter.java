package com.chinasoft.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by YE on 2018/5/24.
 */
@WebFilter(filterName = "IPFilter" /*,urlPatterns = {"/*"}*/)
public class IPFilter implements Filter {
    List<String> list;
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        String ip = req.getRemoteAddr();
        if(list.contains(ip)){
            HttpServletResponse response = (HttpServletResponse) resp;
            response.setContentType("text/html;charset=utf-8");
            response.getWriter().println("禁止傻逼张明远访问!");
        }else{
            chain.doFilter(req, resp);
        }
    }

    public void init(FilterConfig config) throws ServletException {
        list=new ArrayList<>();
        list.add("192.168.21.103");
}

}
