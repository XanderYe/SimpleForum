package com.chinasoft.controller;

import com.chinasoft.utils.VerifyCodeUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by YE on 2018/5/9.
 */
@WebServlet(name = "AuthImageServlet", urlPatterns = {"/authImage"})
public class AuthImageServlet extends HttpServlet {
    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setHeader("Pragma", "No-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        response.setContentType("image/jpeg");
        request.getSession().removeAttribute("vercode");
        String code = VerifyCodeUtils.generateVerifyCode(4);
        request.getSession().setAttribute("vercode", code);
        //生成图片
        int w = 100, h = 30;
        VerifyCodeUtils.outputImage(w, h, response.getOutputStream(), code);
    }
}
