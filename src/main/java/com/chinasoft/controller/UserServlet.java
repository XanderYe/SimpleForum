package com.chinasoft.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONArray;
import com.chinasoft.dao.UserDAO;
import com.chinasoft.dao.Impl.UserDAOImpl;
import com.chinasoft.entity.User;

/**
 * controller implementation class UserServlet
 */
@WebServlet(name = "UserServlet", urlPatterns = {"/user"})
public class UserServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserDAO userDAO = new UserDAOImpl();
        User user = null;
        PrintWriter out = response.getWriter();
        List<User> list = null;
        String userop = request.getParameter("userop");
        if ("login".equals(userop)) {
            String uname = request.getParameter("username");
            String upassword = request.getParameter("password");
            user = new User();
            user.setUsername(uname);
            user.setPassword(upassword);
            if (userDAO.isValid(user)) {
                user = userDAO.selectUser(uname);
                request.getSession().setAttribute("user", user);
                response.sendRedirect("index.jsp");
            } else {
                out.print("<script>alert('用户名或密码错误！');history.go(-1);</script>");
            }
        } else if ("logout".equals(userop)) {
            request.getSession().invalidate();
            out.print("<script>location.href=document.referrer;</script>");
        } else if ("register".equals(userop)) {
            String username = request.getParameter("username");
            String nickname = request.getParameter("nickname");
            String password = request.getParameter("password");
            if (!userDAO.isExist(username)) {
                user = new User();
                user.setUsername(username);
                user.setNickname(nickname);
                user.setPassword(password);
                if (userDAO.addUser(user) > 0) {
                    out.print("<script>alert('注册成功！');location.href='login.jsp'</script>");
                } else {
                    out.print("<script>alert('注册失败');location.href='register.jsp'</script>");
                }
            } else {
                out.print("<script>alert('用户名已存在！');location.href='register.jsp'</script>");
            }
        } else if ("getall".equals(userop)) {
            list = userDAO.getAllUsers();
            String users = JSONArray.toJSONString(list);
            out.print(users);
        } else if ("delete".equals(userop)) {
            int uid = Integer.parseInt(request.getParameter("id"));
            out.print(userDAO.deleteUser(uid));
        } else if ("update".equals(userop)) {
            user = new User();
            user.setUid(Integer.parseInt(request.getParameter("uid")));
            user.setUsername(request.getParameter("username"));
            user.setNickname(request.getParameter("nickname"));
            user.setPassword(request.getParameter("password"));
            user.setEmail(request.getParameter("email"));
            out.print(userDAO.updateUserByAdmin(user));
        } else if ("updateall".equals(userop)) {
            user = new User();
            int uid=Integer.parseInt(request.getParameter("uid"));
            user.setUid(uid);
            user=userDAO.selectUser(uid);
            user.setNickname(request.getParameter("nickname"));
            user.setGender(request.getParameter("gender"));
            user.setEmail(request.getParameter("email"));
            user.setBirthday(request.getParameter("birthday"));
            user.setAddress(request.getParameter("address"));
            if(userDAO.updateUser(user)>0){
                out.print("<script>alert('更新成功！');history.go(-1);</script>");
            }else{
                out.print("<script>alert('更新失败！');history.go(-1);</script>");
            }
        }else if ("getuser".equals(userop)) {
            user = new User();
            int uid=Integer.parseInt(request.getParameter("uid"));
            user=userDAO.selectUser(uid);
            request.setAttribute("user",user);
            request.getRequestDispatcher("home.jsp").forward(request,response);
        } else if ("check".equals(userop)) {
            String username=request.getParameter("username");
            out.print(userDAO.isExist(username));
        }else if ("changepwd".equals(userop)) {
            try{
                user= (User) request.getSession().getAttribute("user");
                String old=request.getParameter("oldpwd");
                if(user.getPassword().equals(old)){
                    String password=request.getParameter("password");
                    user.setPassword(password);
                    if(userDAO.updatePwd(user)>0){
                        request.getSession().invalidate();
                        out.print("<script>alert('修改密码成功！');location.href='login.jsp';</script>");
                    }else{
                        out.print("<script>alert('修改密码失败！');history.go(-1);</script>");
                    }
                }else{
                    out.print("<script>alert('旧密码错误！');history.go(-1);</script>");
                }

            }catch (Exception e){
                response.sendRedirect("login.jsp");
            }
        }

    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
