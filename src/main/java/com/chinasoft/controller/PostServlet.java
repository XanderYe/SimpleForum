package com.chinasoft.controller;

import com.chinasoft.dao.CommentDAO;
import com.chinasoft.dao.Impl.*;
import com.chinasoft.dao.SectionDAO;
import com.chinasoft.dao.TopicDAO;
import com.chinasoft.dao.UserDAO;
import com.chinasoft.entity.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "PostServlet", urlPatterns = {"/post"})
public class PostServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out=response.getWriter();
        String postop=request.getParameter("postop");
        PostDAOImpl postDAO=new PostDAOImpl();
        TopicDAO topicDAO=new TopicDAOImpl();
        UserDAO userDAO=new UserDAOImpl();
        SectionDAO sectionDAO=new SectionDAOImpl();
        CommentDAO commentDAO=new CommentDAOImpl();
        int length=10;
        Post post=null;
        List<Post> postList=null;
        List<Section> sectionList=null;
        if("getposts".equals(postop)){
            int sid= Integer.parseInt(request.getParameter("sid"));
            int tid=Integer.parseInt(request.getParameter("tid"));
            int page=Integer.parseInt(request.getParameter("page"));
            int maxpage=0;
            if(tid==0){
                maxpage =postDAO.getPostsCountBySid(sid,length);
                postList=postDAO.getPostsBySid(sid,(page-1)*length,length);
            }else{
                maxpage =postDAO.getPostsCountByTid(tid,length);
                postList=postDAO.getPostsByTid(tid,(page-1)*length,length);
            }
            Section section=sectionDAO.selectSection(sid);
            request.setAttribute("page",page);
            request.setAttribute("maxpage",maxpage);
            request.setAttribute("section",section);
            request.setAttribute("posts",postList);
            request.getRequestDispatcher("post.jsp").forward(request,response);
        }else if("index".equals(postop)){
            postList=postDAO.getAllPosts(0,length);
            sectionList=sectionDAO.getAllSections();
            request.setAttribute("posts",postList);
            request.setAttribute("sections",sectionList);
            request.getRequestDispatcher("index.jsp").forward(request,response);
        }else if("addpost".equals(postop)){
            String code=request.getParameter("code");
            String vercode= (String) request.getSession().getAttribute("vercode");
            if(!code.toLowerCase().equals(vercode.toLowerCase())){
                out.print("verfail");
            }else{
                post=new Post();
                post.setSid(Integer.parseInt(request.getParameter("sid")));
                Topic topic=topicDAO.selectTopic(Integer.parseInt(request.getParameter("tid")));
                post.setTopic(topic);
                post.setPtitle(request.getParameter("title"));
                User user=userDAO.selectUser(Integer.parseInt(request.getParameter("uid")));
                post.setUser(user);
                post.setPcreatetime(request.getParameter("createtime"));
                int pid=postDAO.addPost(post);
                if(pid!=0){
                    Comment comment=new Comment();
                    comment.setPid(pid);
                    comment.setCfloor(1);
                    comment.setUser(user);
                    comment.setCcontent(request.getParameter("content"));
                    comment.setCtime(request.getParameter("createtime"));
                    commentDAO.addComment(comment);
                    out.print("true");
                }else{
                    out.print("false");
                }
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);

    }
}
